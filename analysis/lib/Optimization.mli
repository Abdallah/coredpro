type proof_direction = Both | Implies | Implied

val print_proof_direction : proof_direction -> string
val make : int -> Ast.T.program -> Ast.T.program
