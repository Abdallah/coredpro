open Printf

module FOL = FirstOrder

type program = (string * FirstOrder.t) list

module type S = sig
  val measure_pred : Pred.t -> Pred.t option
  val program : Pred.t list -> Ast.T.program -> program
end

type 'a branch = { vars : FOL.tvariable list;
                   equal : Ast.T.term list;
                   hyps : Ast.T.topliteral list }
(*type 'a program = (Pred.t, 'a) branch list Pred.Map.t*)

let add_clause accu clause =
  let (head, body) = clause in
  let (pred, args) = head in
  let vars = Ast.variables_rule (head, body) in
  let vars = List.map (fun v -> v, FOL.Individual) vars in
  let branch =
  { vars;
    equal = args;
    hyps = body } in
  Pred.Map.update pred (List.cons branch) [] accu

let completion (inputs : Pred.t list) (prog : Ast.T.program) : Pred.t branch list Pred.Map.t =
  let inputs = Pred.Set.of_list inputs in
  let preds = Pred.Set.diff (Desugared.predicates prog) inputs in
  let map = Pred.Map.from_set (fun _ -> []) preds in
  List.fold_left add_clause map prog

module Clark =
struct

let measure_pred = function
  | _ -> None

let literal : Ast.T.literal -> FOL.t = function
  | Ast.T.Okay atom -> FOL.Atom atom
  | Ast.T.Negation atom -> FOL.Nega (FOL.Atom atom)
  | Ast.T.Predefined p -> FOL.Predef p
let topliteral = function
  | Ast.T.Lit l -> literal l
let branch b =
  let hyps = List.map topliteral b.hyps in
  let equals =
    let print_one i v = FOL.Equa (Ast.T.Expr (Ast.T.Variable (sprintf "%d" i)), v) in
    let (_, eqs) = List.fold_left (fun (i, acc) v -> (i + 1, print_one i v :: acc)) (0, []) b.equal in
    eqs in
(*  FOL.Fora (b.unive, FOL.Exis (b.exist, FOL.Conj (equals @ hyps)))*)
  FOL.Exis (b.vars, FOL.Conj (equals @ hyps))

let equivalences inputs (prog : Ast.T.program) =
  let compl = completion inputs prog in
  let binds = Pred.Map.bindings compl in
  let print_one (pred, bs) =
    let variables = List.map (sprintf "%d") (Utils.PList.range 0 (Ast.arity_pred pred)) in
    let tvars = List.map (fun v -> v, FOL.Individual) variables in
    let left_hand = FOL.Atom (pred, List.map Ast.variable variables) in
    let right_hand = FOL.Disj (List.map branch bs) in
    (pred, tvars, left_hand, right_hand) in
  List.map print_one binds

let program inputs (prog : Ast.T.program) =
  let prog = equivalences inputs prog in
  let print_one (pred, vars, left, right) = FOL.(Print.predicate pred, Fora (vars, Equi (left, right))) in
  List.map print_one prog

end

module Ordered =
struct

let make_measure_keyword = function
  | Ast.T.Constant (str, arity) -> Ast.T.Constant ("measure_" ^ str, arity)
  | kw -> Ast.T.Constant ("measure_" ^ Ast.Print.keyword kw, Ast.arity_kw kw)

let measure_pred (n, kw) = Some (n, make_measure_keyword kw)

let make_measure (pred, vars) = try (Option.get (measure_pred pred), vars) with Invalid_argument _ -> assert false

let get_positive_lit = function
  | Ast.T.Okay atom -> Some atom
  | Ast.T.Negation _ -> None
  | Ast.T.Predefined _ -> None
let get_positive = function
  | Ast.T.Lit l -> get_positive_lit l
let branch (pred, vars) b =
  let hyps = List.map Clark.topliteral b.hyps in
  let positive = List.filter_map get_positive b.hyps in
  let measure_head = make_measure (pred, vars) in
  let measures = List.map (fun x -> FOL.Lessp (make_measure x, measure_head)) positive in
  let equals =
    let print_one i v = FOL.Equa (Ast.T.Expr (Ast.T.Variable (sprintf "%d" i)), v) in
    let (_, eqs) = List.fold_left (fun (i, acc) v -> (i + 1, print_one i v :: acc)) (0, []) b.equal in
    eqs in
(*  FOL.Fora (b.unive, FOL.Exis (b.exist, FOL.Conj (equals @ hyps)))*)
  (FOL.Exis (b.vars, FOL.Conj (equals @ hyps @ measures)),
   FOL.Exis (b.vars, FOL.Conj (equals @ hyps)))

let program inputs prog =
  let compl = completion inputs prog in
  let binds = Pred.Map.bindings compl in
  let print_one (pred, bs) =
    let variables = List.map (sprintf "%d") (Utils.PList.range 0 (Ast.arity_pred pred)) in
    let tvars = List.map (fun v -> v, FOL.Individual) variables in
    let vars = List.map Ast.variable variables in
    let left_hand = FOL.Atom (pred, vars) in
    let (branchesl, branchesr) = List.split (List.map (branch (pred, vars)) bs) in
    let clausel =
      FOL.(Print.predicate pred ^ "l",
           Fora (tvars, Impl (left_hand, Disj branchesl)))
    and clauser =
      FOL.(Print.predicate pred ^ "r",
           Fora (tvars, Impl (Disj branchesr, left_hand))) in
    [clausel; clauser] in
  List.concat_map print_one binds
end

module Embedded =
struct

let m' = "n"
let atom m (pred, args) = FOL.Atom (pred, Ast.T.Expr (Ast.T.Variable m) :: args)
let literal' m = function
  | Ast.T.Okay a -> FOL.(Exis ([m', Int], Conj [Less (Either.Right 0, Either.Left m'); Less (Either.Left m', m); atom m' a]))
  | Ast.T.Negation a -> FOL.(Fora ([m', Int], Impl (Less (Either.Right 0, Either.Left m'), Nega (atom m' a))))
  | Ast.T.Predefined p -> FOL.Predef p
let topliteral' m = function
  | Ast.T.Lit l -> literal' m l
let branch' measure b =
  let hyps = List.map (topliteral' measure) b.hyps in
  let equals =
    let print_one i v = FOL.Equa (Ast.T.Expr (Ast.T.Variable (sprintf "%d" i)), v) in
    let (_, eqs) = List.fold_left (fun (i, acc) v -> (i + 1, print_one i v :: acc)) (0, []) b.equal in
    eqs in
(*  FOL.Fora (b.unive, FOL.Exis (b.exist, FOL.Conj (equals @ hyps)))*)
  FOL.Exis (b.vars, FOL.Conj (equals @ hyps))

let program_new inputs prog =
  let compl = completion inputs prog in
  let binds = Pred.Map.bindings compl in
  let print_one (pred, bs) =
    let measure = "M" in
    let tmeasure = measure, FOL.Int in
    let variables = List.map (sprintf "%d") (Utils.PList.range 0 (Ast.arity_pred pred)) in
    let tvars = List.map (fun v -> v, FOL.Individual) variables in
    let left_hand = FOL.Atom (pred, Ast.T.Expr (Ast.T.Variable measure) :: List.map Ast.variable variables) in
    let right_hand = FOL.Disj (List.map (branch' (Either.Left measure)) bs) in
    let nonneg_measure = FOL.Less (Either.Right 0, Either.Left measure) in
    FOL.(Print.predicate pred,
         Fora (tvars @ [tmeasure], Impl (nonneg_measure, Equi (left_hand, right_hand)))) in
  List.map print_one binds
end

module Unidirectional =
struct

let measure_pred = function
  | _ -> None

let clause (head, body) =
  let (pred, _) = head in
  let vars = Ast.variables_rule (head, body) in
  let univ_vars = Ast.variables_atomic head in
  let exis_vars = Utils.PList.subtract vars univ_vars in
  let univ_vars = Utils.PList.subtract vars exis_vars in
  let exis = List.map (fun v -> v, FOL.Individual) exis_vars in
  let univ = List.map (fun v -> v, FOL.Individual) univ_vars in
  let hyps = List.map Clark.topliteral body in
  let formula = FOL.Fora (univ, Impl (Exis (exis, Conj hyps), Atom head)) in
  let name = FOL.Print.predicate pred in
  (name, formula)
(*  FOL.{ name =;
        role = Axiom;
        body = FLogic formula }*)

let program _ prog =
  List.map clause prog

end
