open Printf

module C = Combine
module CT = C.T
module CG = Combine.GuidedVerify
module CS = Combine.Solve
module CC = Combine.Check
module CR = Combine.Run

type clause =
  | Cardinality of (string option * CT.atomic * string option * CT.topliteral list)
  | Constant of (string * int)
  | Constraint of (CT.topliteral list) | Generator of (CT.atomic * CT.topliteral list) | Rule of (CT.atomic * CT.topliteral list)
  | Quantifier of (Ast.T.expr * CT.atomic * CT.topliteral list)
  | ShowMessage of (string * CT.topliteral list) | ShowNothing

module Print =
struct
let escape = Utils.MString.escape ['_'; '+'; '-'; '<'; '>'; '='; '/'; '\''; '\\'] '_'

let binop = function
  | Ast.T.Add -> "+"
  | Div -> "/"
  | Mult -> "*"
  | Log -> assert false
  | Mod -> "\\"
  | Pow -> "**"
  | Sub -> "-"

let variable v =
  let v = escape v in
  let uv = String.capitalize_ascii v in
  if uv = v then v else "V" ^ v

let symbol fct = escape (fst fct)

let predicate = symbol


let rec expr = function
    | Ast.T.BinOp (bo, e1, e2) -> sprintf "%s %s %s" (inner_expr e1) (binop bo) (inner_expr e2)
    | Variable _ | Int _ as e -> inner_expr e
and inner_expr = function
    | Ast.T.Variable n -> variable n
    | Int i -> Utils.Print.int i
    | BinOp _ as e -> sprintf "(%s)" (expr e)

let rec term = function
  | Ast.T.Fact (symb, []) -> symbol symb
  | Ast.T.Fact (symb, ts) -> sprintf "%s%s" (symbol symb) (terms ts)
  | Ast.T.Expr e -> expr e
and terms ts = Utils.Print.list' "(" "," ")" term ts
let atomic (pred, ts) = match ts with
  | [] -> predicate pred
  | _ :: _ -> sprintf "%s%s" (predicate pred) (terms ts)

let comparator = function
  | Ast.T.Eq -> "="
  | Ast.T.Neq -> "!="
  | Ast.T.LessEq -> "<="
  | Ast.T.LessThan -> "<"
  | Ast.T.MoreEq -> ">="
  | Ast.T.MoreThan -> ">"

let predefined = function
  | Ast.T.Unary _ -> assert false
  | Ast.T.Relation (b, t1, t2) -> sprintf "%s %s %s" (term t1) (comparator b) (term t2)
let literal = function
  | CT.Okay     atom -> atomic atom
  | CT.Negation atom -> sprintf "not %s" (atomic atom)
  | CT.Predefined p -> predefined p
let topliteral = function
  | CT.Lit l -> literal l

let topliterals = Utils.Print.list' "" ", " "" topliteral

let rule (head, body) =
  let head = atomic head in
  if body = [] then head ^ "."
  else sprintf "%s :- %s." head (topliterals body)

let generator (head, body) =
  let head = atomic head in
  if body = [] then sprintf "{ %s }." head
  else sprintf "{ %s } :- %s." head (topliterals body)

let pconstraint body =
  assert (body <> []);
  sprintf ":- %s." (topliterals body)

let quantifier (depth, head, body) =
  sprintf "_quantify(%s, %s) :- %s." (expr depth) (atomic head) (topliterals body)

let constant (name, value) = sprintf "#const %s=%d." name value
let show_message (message, body) = sprintf "#show %s : %s." message (topliterals body)

let cardinality_constraint (lo, atom, up, condition) =
  let lo = Option.value lo ~default:""
  and up = Option.value up ~default:"0" in
  let cond = if condition <> [] then ", " ^ topliterals condition else "" in
  let clo = sprintf ":- { %s } %s, %s >= 0%s." (atomic atom) lo lo cond
  and cup = sprintf ":- %s { %s }, %s > 0%s." up (atomic atom) up cond in
  clo ^ "\n" ^ cup

let clause = function
  | Cardinality c -> cardinality_constraint c
  | Constant c -> constant c
  | Constraint c -> pconstraint c
  | Rule r -> rule r
  | Generator g -> generator g
  | ShowMessage m -> show_message m
  | ShowNothing -> "#show."
  | Quantifier q -> quantifier q

let program prog = Utils.Print.unlines clause prog
end

let positive_lit atom = CT.(Lit (CT.Okay atom))
let negative_lit atom = CT.(Lit (CT.Negation atom))

let direct_lit = positive_lit CG.direct
let revers = negative_lit CG.direct

let constraints = List.map (fun l -> Constraint l)
let generators = List.map (fun l -> Generator l)
let rules = List.map (fun l -> Rule l)
let showmessages = List.map (fun l -> ShowMessage l)

module GuidedVerify =
struct
  let constraints =
    let both_there = [direct_lit; positive_lit CG.ouput_certificate]
    and neither = [revers; positive_lit CG.input_certificate]
    and force_direct = [direct_lit; negative_lit CG.input_certificate]
    and force_revers = [revers; negative_lit CG.ouput_certificate] in
    constraints [both_there; neither; force_direct; force_revers]

  let generators =
    let instance = (CG.input_instance, [positive_lit CG.input_domain]) in
    let direct_g = (CG.input_solution, [direct_lit; positive_lit CG.input_candidate])
    and revers_g = (CG.ouput_solution, [revers; positive_lit CG.ouput_candidate]) in
    generators [(CG.direct, []); instance; direct_g; revers_g]

  let cardinality =
    let make_card cond name atom =
      let up = sprintf "up_%s" name
      and lo = sprintf "lo_%s" name in
      let cup = Constant (up, 0)
      and clo = Constant (lo, 0)
      and cardi = Cardinality (Some lo, atom, Some up, cond) in
      [cup; clo; cardi] in
    let i_card  = make_card [] "inp_inst" CG.input_instance
    and c1_card = make_card [direct_lit] "inp_solu" CG.input_solution
    and c2_card = make_card [revers] "out_solu" CG.ouput_solution in
    i_card @ c1_card @ c2_card

  let show =
    let direction = [("direct", [direct_lit]); ("reverse", [revers])] in
    let instance  =
      [(sprintf "in_base(%s)" C.x, [positive_lit CG.input_basis]);
       (sprintf "in_inst(%s)" C.x, [positive_lit CG.input_instance]);
       (sprintf "ou_inst(%s)" C.x, [positive_lit CG.ouput_instance]);
       (sprintf "in_solu(%s)" C.x, [positive_lit CG.input_solution]);
       (sprintf "ou_solu(%s)" C.x, [positive_lit CG.ouput_solution])] in
    ShowNothing :: showmessages (direction @ instance)
end

(*module Check =
struct
end*)

(*let instance_size = "r_size"*)
let default_size = 3

let generate_base =
  let const = Print.clause (Constant ("r_base", default_size)) in
  let set = sprintf "%s(X,1..r_base) :- %s." (Print.predicate CG.input_base) (Print.topliteral (positive_lit CG.input_basis)) in
  const ^ "\n" ^ set

let guided_verify (p1 : Ast.T.problem) r p2 =
  let spec = { Ast.T.input = p1; reduction = r; output = p2 } in
  let clauses = rules (CG.program spec) in
  sprintf "%s\n%s\n" generate_base (Print.program (GuidedVerify.generators @ GuidedVerify.constraints @ GuidedVerify.cardinality @ clauses @ GuidedVerify.show))


let verify = guided_verify
(*let prog ((ta, tr, tb), p) =
  assert (tr = []);
  sprintf "%s\n%s\n%s\n%s\n%s\n%s\n\n%s" (define_size ta) generator contradiction choose_direct show (Print.program (ta @ tr @ tb)) (Print.program p)*)

let solve p i =
  let constr = Constraint [negative_lit CS.certificate] in
  let genera = Generator (CS.solution, [positive_lit CS.candidate]) in
  let rules = rules (CS.program p i) in
  let solut = ShowMessage (Combine.x, [positive_lit CS.solution]) in
  let show = [ShowNothing; solut] in
  sprintf "%s\n" (Print.program (rules @ genera :: constr :: show))

let check p i s =
  let rules = rules (CC.program p i s) in
  eprintf "missing bad domain detection\n%!";
  let show = [ShowNothing; ShowMessage ("correct", [positive_lit CC.certificate]); ShowMessage ("incorrect", [negative_lit CC.certificate])] in
  sprintf "%s\n" (Print.program (rules @ show))

let run a i =
  let rules = rules (CR.program a i) in
  let solut = ShowMessage (Combine.x, [positive_lit CR.solution]) in
  let show = [ShowNothing; solut] in
  sprintf "%s\n" (Print.program (rules @ show))

