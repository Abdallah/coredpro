val inline_aux : int -> Ast.T.program -> Ast.T.program -> Ast.T.program
val program : int -> Ast.T.program -> Ast.T.program

(*
module Ground :
sig
  val program : int * int -> (Ast.T.predicate, Ast.T.ground) Desugared.program -> (Ast.T.predicate, Ast.T.free) Desugared.program
end
*)
