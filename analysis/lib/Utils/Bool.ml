let iff a b = (a && b) || ((not a) && (not b))
let imply a b = b || (not a)

let list_or l = List.exists (fun x -> x) l
let list_and l = List.for_all (fun x -> x) l
