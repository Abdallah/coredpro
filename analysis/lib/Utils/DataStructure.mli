module type S =
sig
  type t
  val print : t -> string

  module Set :
  sig
    include Set.S with type elt = t
    val memr : t -> elt -> bool
    val mapl : (elt -> 'a) -> t -> 'a list
    val maps : (elt -> 'a list) -> t -> 'a list
    val unions : t list -> t
    val union_maps : (elt -> t) -> t -> t
    val maps_union : ('a -> t) -> 'a list -> t
    val inters : t list -> t
    val maps_inter : ('a -> t) -> 'a list -> t
    val filter_map' : (elt -> elt option) -> t -> t
    val filter_mapl : (elt -> 'a option) -> t -> 'a list
    val fold_left : ('a -> elt -> 'a) -> 'a -> t -> 'a
    val print_elt : elt -> string
    val print' : string -> string -> string -> (elt -> string) -> t -> string
    val print : t -> string
    val unlines : (elt -> string) -> t -> string
    val unspaces : t -> string
  end

  module Map :
  sig
    include Map.S with type key = t
    val init : 'a -> key list -> 'a t
    val memr : 'a t -> key -> bool
    val findr : 'a t -> key -> 'a
    val find_default : key -> 'a -> 'a t -> 'a
    val find_anywhere' : (key -> 'a -> 'b option) -> 'a t -> 'b option
    val update : key -> ('a -> 'a) -> 'a -> 'a t -> 'a t
    val keys : 'a t -> key list
    val keys' : 'a t -> Set.t
    val values : 'a t -> 'a list
    val from_list : (key -> 'a) -> key list -> 'a t
    val from_bindings : (key * 'a) list -> 'a t
    val from_set : (key -> 'a) -> Set.t -> 'a t
    val fold_map : ('c -> 'a -> 'c * 'b) -> 'c -> 'a t -> 'c * 'b t
    val print_key : key -> string
    val print' : string -> string -> string -> (key * 'a -> string) -> 'a t -> string
    val print : ('a -> string) -> 'a t -> string
    val unlines : ('a -> string) -> 'a t -> string
    val unspaces : ('a -> string) -> 'a t -> string
    val exists : (key -> 'a -> bool) -> 'a t -> bool
    val union_no_erase : 'a t -> 'a t -> 'a t option
    val unions : ('a -> 'a -> 'a) -> 'a t list -> 'a t
    val inter : ('a -> 'a -> 'a) -> 'a t -> 'a t -> 'a t
    val cross_products : 'a list t -> 'a t list
    val merges : ('a list -> 'b) -> 'a t list -> 'b t
  end

  module DisjointSet :
  sig
    type elt = t
    type t

    val empty : t
    val find : elt -> t -> elt
    val union : elt -> elt -> t -> t
    val unions : elt list -> t -> t
    val bound : elt -> elt -> t -> bool
    val sets : t -> elt list list
    val print : t -> string
  end

  module SCC :
  sig
    val tarjan : Set.t Map.t -> Set.t list
  end
end

module F (Ord : sig include Set.OrderedType val print : t -> string end) : S with type t = Ord.t
