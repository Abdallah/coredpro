type t = String.t

val to_list : t -> char list
val maps : (char -> t) -> t -> t
val separate : t -> int -> t * t
val replace_many : t -> (char * string) list -> t
val escape : char list -> char -> t -> t
val repeat : t -> int -> t
val starts_with : t -> t -> bool
val ends_with : t -> t -> bool
val starts_lowercase : t -> bool
val only_letters : t -> bool
