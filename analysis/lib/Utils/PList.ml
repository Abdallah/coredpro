open Printf

let uniques l = List.rev (List.fold_left (fun accu x -> if List.mem x accu then accu else x :: accu) [] l)

let rec no_doubles = function
  | [] -> true
  | a :: r -> not (List.mem a r) && no_doubles r

let range m n =
  let rec aux accu x y = if x >= y then accu else aux ((y - 1) :: accu) x (y - 1) in
  aux [] m n

let mapi f i l =
  let aux (results, i) elt = (f i elt :: results, i + 1) in
  let (results, _) = List.fold_left aux ([], i) l in
  List.rev results
let rec foralli f i = function
  | [] -> true
  | a :: r -> f i a && foralli f (i + 1) r

let subtract l1 l2 =
  let add accu x = if List.mem x l2 then accu else x :: accu in
  let sub = List.fold_left add [] l1 in
  List.rev sub

let split l =
  let aux (bs, cs) (b, c) = (b :: bs, c :: cs) in
  let bs, cs = List.fold_left aux ([], []) l in
  (List.rev bs, List.rev cs)

let split_map f l =
  let aux (bs, cs) a =
    let (b, c) = f a in
    (b :: bs, c :: cs) in
  let bs, cs = List.fold_left aux ([], []) l in
  (List.rev bs, List.rev cs)

let split_mapi f i l =
  let aux (bs, cs, i) a =
    let (b, c) = f i a in
    (b :: bs, c :: cs, i + 1) in
  let bs, cs, _ = List.fold_left aux ([], [], i) l in
  (List.rev bs, List.rev cs)

let combine l1 l2 =
  let rec aux results bs cs = match bs, cs with
    | [], [] -> List.rev results
    | b :: bs', c :: cs' -> aux ((b, c) :: results) bs' cs'
    | _ :: _, [] | [], _ :: _ -> assert false in
  aux [] l1 l2

let fold_right f l accu =
  let g accu x = f x accu in
  List.fold_left g accu (List.rev l)

let flatten l = List.rev (List.fold_left (Fun.flip List.rev_append) [] l)

let filter_maps f l = flatten (List.filter_map f l)

let mapis f i l = flatten (mapi f i l)

let replace_one f l =
  let rec aux accu = function
    | [] -> None
    | a :: r -> match f a with
      | None -> aux (a :: accu) r
      | Some b -> Some (List.rev_append accu (b :: r)) in
  aux [] l

let queue l a = List.rev_append (List.rev l) [a]

let rec fold_stop f stop accu l =
  if stop accu then (accu, l)
  else match l with
    | [] -> (accu, l)
    | a :: r -> fold_stop f stop (f accu a) r

let rec fold_stop' (f : 'b -> 'a -> 'b option) accu l = match accu, l with
  | None, _ | _, [] -> (accu, l)
  | Some b, a :: r -> fold_stop' f (f b a) r

let fold_first fold = function
  | [] -> assert false
  | a :: r -> List.fold_left fold a r

let append l1 l2 = List.rev_append (List.rev l1) l2
let map f l = List.rev (List.rev_map f l)

let find_first f l =
  let rec aux accu = function
    | [] -> None
    | a :: r when f a -> Some (accu, a, r)
    | a :: r -> aux (a :: accu) r in
  aux [] l

let find_first' f l =
  let rec aux accu = function
    | [] -> None
    | a :: r -> match f a with
      | None -> aux (a :: accu) r
      | Some b -> Some (accu, b, r) in
  aux [] l

let except f l = List.find_opt (fun x -> not (f x)) l

let max cmp l =
  let rec aux current = function
    | [] -> current
    | a :: r -> if cmp a current > 0 then aux a r else aux current r in
  match l with
  | [] -> assert false
  | a :: r -> aux a r

let rec nth i = function
  | [] -> assert false
  | a :: r -> if i <= 0 then a else nth (i - 1) r

let cross_product_map f l1 l2 =
  let aux accu a =
    let la2 = List.rev_map (f a) l2 in
    List.rev_append la2 accu in
  let l12 = List.fold_left aux [] (List.rev l1) in
  List.rev l12
let cross_product l1 l2 = cross_product_map (fun a b -> (a, b)) l1 l2

let cross_products l =
  let aux accu elts = flatten (map (fun elt -> map (List.cons elt) accu) elts) in
  let results = List.fold_left aux [[]] l in
  map List.rev results

let union l1 l2 =
  let l1' = List.filter (fun e -> not (List.mem e l2)) l1 in
  append l1' l2


let rec is_prefix path1 path2 = match path1, path2 with
  | [], _ -> Some path2
  | _ :: _, [] -> None
  | h1 :: r1, h2 :: r2 -> if h1 = h2 then is_prefix r1 r2 else None

let random_element l = nth (Random.int (List.length l)) l

let memr l a = List.mem a l

let intersect l1 l2 = List.filter (memr l2) l1

let filter_out f l = List.filter (fun x -> not (f x)) l

let count f l = List.length (List.filter f l)
