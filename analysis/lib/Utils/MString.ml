open Printf

type t = String.t

let to_list string =
  let l = ref [] in
  String.iter (fun char -> l := char :: !l) string;
  List.rev !l

let separate original number =
  let before = String.sub original 0 number
  and after = String.sub original (number + 1) (String.length original - number - 1) in
  (before, after)

let maps f string =
  let chars = to_list string in
  String.concat "" (List.map f chars)

let replace_many original map =
  let f x = try List.assoc x map with Not_found -> String.make 1 x in
  maps f original

let repeat s nb =
  let rec aux accu n =
    if n <= 0 then accu
    else aux (s ^ accu) (n - 1) in
  aux "" nb

let starts_with main sub =
  String.length main >= String.length sub && String.sub main 0 (String.length sub) = sub

let ends_with main sub =
  String.length main >= String.length sub && String.sub main (String.length main - String.length sub) (String.length sub) = sub

let escape forbidden_chars replacement =
(*  assert (List.mem replacement forbidden_chars);*)
  assert (not (List.mem 'e' forbidden_chars));
  let rep = String.make 1 replacement in
  let f i forbidden = (forbidden, sprintf "e%d%s" i rep) in
  let map = PList.mapi f 0 forbidden_chars in
  (fun x -> replace_many x map)

let starts_lowercase string =
  let c = Char.code string.[0] in
  c >= 97 && c <= 122

let only_letters string =
  let fail_not_letter c =
    let code = Char.code c in
    if not (code >= 65 && code <= 90) || (code >= 97 && code <= 122) then raise Exit in
  try String.iter fail_not_letter string; true
  with Exit -> false
