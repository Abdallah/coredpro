let minute = 60.
and hour = 3_600.

let iof = int_of_float
let time () = Sys.time ()

type timer = { start_time : float;
               duration : float }

let start t = { start_time = time ();
                duration = t }

let elapsed t = time () -. t.start_time >= t.duration

type chrono = { mutable chrono_start : float }

let chrono_make () = { chrono_start = time () }
let chrono_restart c = c.chrono_start <- time ()
let chrono_elapsed c = time () -. c.chrono_start

let measure f a =
  let c = chrono_make () in
  let b = f a in
  (b, chrono_elapsed c)

let measure_several iter f a =
  let c = chrono_make () in
  for i = 1 to (iter - 1) do
    ignore (f a)
  done;
  let b = f a in
  (b, chrono_elapsed c)

let children_chrono_make () = Unix.times ()
let children_chrono_elapsed oldt =
  let newt = Unix.times () in
  newt.Unix.tms_utime -. oldt.Unix.tms_utime
  +. newt.Unix.tms_cutime -. oldt.Unix.tms_cutime
  +. newt.Unix.tms_stime -. oldt.Unix.tms_stime
  +. newt.Unix.tms_cstime -. oldt.Unix.tms_cstime 
