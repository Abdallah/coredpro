open Printf

let cons a r = a :: r

module F (Ord : sig include Set.OrderedType val print : t -> string end) =
struct
  type t = Ord.t
  let print = Ord.print

  module Set =
  struct
    include Set.Make (Ord)

    let memr t e = mem e t
    let mapl f t = List.rev_map f (elements t)
    let maps f t = List.concat_map f (elements t)
    let unions l = List.fold_left union empty l
    let union_maps f t = unions (mapl f t)
    let maps_union f l = unions (PList.map f l)
    let inters = function
      | [] -> assert false;
      | a :: r -> List.fold_left inter a r
    let maps_inter f l = inters (PList.map f l)
    let filter_map' f t = fold (fun key accu -> match f key with None -> accu | Some elt -> add elt accu) t empty
    let filter_mapl f t = List.filter_map f (elements t)
    let fold_left f a t = fold (fun key accu -> f accu key) t a
    let print_elt = Ord.print
    let print' d1 d2 d3 pr t = Print.list' d1 d2 d3 pr (elements t)
    let print = print' "{" ", " "}" print_elt
    let unlines = print' "" "\n" ""
    let unspaces = print' "" " " "" print_elt
  end

  module Map =
  struct
    include Map.Make (Ord)

    let init a keys = List.fold_left (fun map key -> add key a map) empty keys
    let memr m f = mem f m
    let findr m f = try find f m with Not_found -> assert false
    let find_default key default map = try find key map with Not_found -> default
    let find_anywhere' (type a) test m =
      let exception Found of a in
      let aux key elt = match test key elt with
          | None -> ()
          | Some a -> raise (Found a) in
      try iter aux m; None with Found a -> Some a
    let update key f default map = add key (f (find_default key default map)) map

    let keys' m = fold (fun k _ acc -> Set.add k acc) m Set.empty
    let keys m = Set.elements (keys' m) (*PList.uniques (fold (fun k _ acc -> k :: acc) m [])*)
    let values m = PList.uniques (fold (fun _ value acc -> value :: acc) m [])
    let from_list f keys = List.fold_left (fun map key -> add key (f key) map) empty keys
    let from_bindings binds = List.fold_left (fun map (key, value) -> add key value map) empty binds
    let from_set f set = Set.fold (fun t map -> add t (f t) map) set empty

    let fold_map f c map =
      let aux key elt (acc_c, acc_map) =
        let c', b = f acc_c elt in
        (c', add key b acc_map) in
      fold aux map (c, empty)

    let print_key = Ord.print
    let print' op del cl pr_pair map = Print.list' op del cl pr_pair (bindings map)
    let print pr_elt map = print' "{" " " "}" (Print.couple print_key pr_elt) map
    let unlines pr_elt map = print' "" "\n" "" (Print.couple print_key pr_elt) map
    let unspaces pr_elt map = print' "" " " "" (Print.couple print_key pr_elt) map

    let exists f m =
      let test k a = if f k a then raise Exit in
      try iter test m; false with Exit -> true

    let union_no_erase m1 m2 =
      let f _ x y = match x, y with
        | None, None -> None
        | Some xx, None -> x
        | None, Some yy -> y
        | Some xx, Some yy -> if xx = yy then x else raise Exit in
      try Some (merge f m1 m2) with
      | Exit -> None

    let unions f = function
      | [] -> assert false
      | hd :: tl -> List.fold_left (union (fun _ x y -> Some (f x y))) hd tl

    let inter f m1 m2 =
      let f' _ x y = match x, y with
        | None, None -> None
        | Some _, None -> None
        | None, Some _ -> None
        | Some xx, Some yy -> Some (f xx yy) in
      merge f' m1 m2

    let cross_products map =
      let aux key list maps = List.concat_map (fun value -> PList.map (add key value) maps) list in
      fold aux map [empty]

    let merges f maps =
      let ks = Set.unions (PList.map keys' maps) in
      let f' key = f (List.filter_map (find_opt key) maps) in
      from_set f' ks
  end

  module DisjointSet =
  struct
    type elt = Ord.t
    type t = elt Map.t

    let empty = Map.empty

    let find_aux orig map =
      let rec aux traversed current =
        let next = Map.find_default current current map in
        if next = current then (next, traversed) else aux (current :: traversed) next in
      let (final, traversed) = aux [] orig in
      (final, List.fold_left (fun accu i -> Map.add i final accu) map traversed)

    let find i map = fst (find_aux i map)

    let union i j map =
      let (iref, map) = find_aux i map in
      let (jref, map) = find_aux j map in
      let small, large = if Ord.compare iref jref < 0 then iref, jref else jref, iref in
      Map.add large small (Map.add small small map)

    let unions is map = match is with
      | [] -> map
      | a :: rest -> List.fold_left (fun m b -> union a b m) map rest

    let bound i j map =
      let iref = find i map
      and jref = find j map in
      iref = jref

    let sets uf_map =
      let keys = Map.keys uf_map in
      let add map key =
        let target = find key uf_map in
        Map.update target (cons key) [] map in
      let res_map = List.fold_left add Map.empty keys in
      Map.values res_map

    let print = Map.print Ord.print
  end

  module SCC =
  struct

    (** Adapted from wikipedia. Complexity can be improved by getting mem in constant time. *)
    let tarjan graph =
      let comps = ref [] in
      let index = ref 0 in
      let stack = Stack.create () in
      let indices = ref Map.empty
      and lowlink = ref Map.empty in
      let rec scc v ws =
        if not (Map.mem v !indices)
        then (indices := Map.add v !index !indices;
              lowlink := Map.add v !index !lowlink;
              incr index;
              Stack.push v stack;
              let inner_loop w =
                scc w (Map.find w graph);
                if MStack.mem w stack then lowlink := Map.add v (min (Map.find v !lowlink) (Map.find w !lowlink)) !lowlink in
              Set.iter inner_loop ws;
              if Map.find v !indices = Map.find v !lowlink
              then comps := Set.of_list (MStack.pop_upto ((=) v) stack) :: !comps) in
      Map.iter scc graph;
      List.rev !comps

  end
end

(*module type S = module type of F (struct type t = unit let compare = compare let print _ = "()" end)*)

module type S =
sig
  type t
  val print : t -> string

  module Set :
  sig
    include Set.S with type elt = t
    val memr : t -> elt -> bool
    val mapl : (elt -> 'a) -> t -> 'a list
    val maps : (elt -> 'a list) -> t -> 'a list
    val unions : t list -> t
    val union_maps : (elt -> t) -> t -> t
    val maps_union : ('a -> t) -> 'a list -> t
    val inters : t list -> t
    val maps_inter : ('a -> t) -> 'a list -> t
    val filter_map' : (elt -> elt option) -> t -> t
    val filter_mapl : (elt -> 'a option) -> t -> 'a list
    val fold_left : ('a -> elt -> 'a) -> 'a -> t -> 'a
    val print_elt : elt -> string
    val print' : string -> string -> string -> (elt -> string) -> t -> string
    val print : t -> string
    val unlines : (elt -> string) -> t -> string
    val unspaces : t -> string
  end

  module Map :
  sig
    include Map.S with type key = t
    val init : 'a -> key list -> 'a t
    val memr : 'a t -> key -> bool
    val findr : 'a t -> key -> 'a
    val find_default : key -> 'a -> 'a t -> 'a
    val find_anywhere' : (key -> 'a -> 'b option) -> 'a t -> 'b option
    val update : key -> ('a -> 'a) -> 'a -> 'a t -> 'a t
    val keys : 'a t -> key list
    val keys' : 'a t -> Set.t
    val values : 'a t -> 'a list
    val from_list : (key -> 'a) -> key list -> 'a t
    val from_bindings : (key * 'a) list -> 'a t
    val from_set : (key -> 'a) -> Set.t -> 'a t
    val fold_map : ('c -> 'a -> 'c * 'b) -> 'c -> 'a t -> 'c * 'b t
    val print_key : key -> string
    val print' : string -> string -> string -> (key * 'a -> string) -> 'a t -> string
    val print : ('a -> string) -> 'a t -> string
    val unlines : ('a -> string) -> 'a t -> string
    val unspaces : ('a -> string) -> 'a t -> string
    val exists : (key -> 'a -> bool) -> 'a t -> bool
    val union_no_erase : 'a t -> 'a t -> 'a t option
    val unions : ('a -> 'a -> 'a) -> 'a t list -> 'a t
    val inter : ('a -> 'a -> 'a) -> 'a t -> 'a t -> 'a t
    val cross_products : 'a list t -> 'a t list
    val merges : ('a list -> 'b) -> 'a t list -> 'b t
  end

  module DisjointSet :
  sig
    type elt = t
    type t

    val empty : t
    val find : elt -> t -> elt
    val union : elt -> elt -> t -> t
    val unions : elt list -> t -> t
    val bound : elt -> elt -> t -> bool
    val sets : t -> elt list list
    val print : t -> string
  end

  module SCC :
  sig
    val tarjan : Set.t Map.t -> Set.t list
  end
end
