(** [uniques l] returns the list of elements of [l] each occuring exactly once according to [(=)].  Stable from the front. O(mn) with m and n the size of the original and final lists.  For instance [uniques [1; 2; 1; 3]] returns [[2; 1; 3]].*)
val uniques : 'a list -> 'a list

(** [no_doubles l] returns [true] when [l] has no element appearing twice. *)
val no_doubles : 'a list -> bool

val range : int -> int -> int list
val mapi : (int -> 'a -> 'b) -> int -> 'a list -> 'b list
val foralli : (int -> 'a -> bool) -> int -> 'a list -> bool

(** [subtract [1; 2; 3] [2; 4] = [1; 3] *)
val subtract : 'a list -> 'a list -> 'a list

(** Tail-recursive. *)
val split : ('a * 'b) list -> 'a list * 'b list
val split_map : ('a -> 'b * 'c) -> 'a list -> 'b list * 'c list
val split_mapi : (int -> 'a -> 'b * 'c) -> int -> 'a list -> 'b list * 'c list

(** Assumes input lists of the same length.*)
val combine : 'a list -> 'b list -> ('a * 'b) list
val fold_right : ('a -> 'b -> 'b) -> 'a list -> 'b -> 'b

(** Tail-recursive. *)
val flatten : 'a list list -> 'a list

(** [filter_maps f l] is equivalent to [List.flatten (filter_map f l)]. Tail-recursive. *)
val filter_maps: ('a -> 'b list option) -> 'a list -> 'b list

(** [mapis f i l] is equivalent to [List.flatten (mapi f i l)]. Tail-recursive. *)
val mapis: (int -> 'a -> 'b list) -> int -> 'a list -> 'b list
val replace_one : ('a -> 'a option) -> 'a list -> 'a list option
val queue : 'a list -> 'a -> 'a list
val fold_stop : ('b -> 'a -> 'b) -> ('b -> bool) -> 'b -> 'a list -> 'b * 'a list
val fold_stop' : ('b -> 'a -> 'b option) -> 'b option -> 'a list -> 'b option * 'a list

(** [fold_first f (a :: r)] is equivalent to [fold_left f a r]. assumes a non-empty list. *)
val fold_first : ('a -> 'a -> 'a) -> 'a list -> 'a

(** Tail-recursive. *)
val append : 'a list -> 'a list -> 'a list

(** Tail-recursive. *)
val map : ('a -> 'b) -> 'a list -> 'b list

(** Dual to [find]: [except is_even [2; 4; 5; 6]] returns [Some 5]. Tail-recursive.*)
val except : ('a -> bool) -> 'a list -> 'a option

(** [find_first ((=) 4) [1; 2; 3; 4; 5; 6; 7; 4; 8]] returns [([3; 2; 1], 4, [5; 6; 7; 4; 8])]. Tail-recursive.*)
val find_first : ('a -> bool) -> 'a list -> ('a list * 'a * 'a list) option
val find_first' : ('a -> 'b option) -> 'a list -> ('a list * 'b * 'a list) option

(** Assumes a non-empty list.  Returns the first occurence among equal elements.  Tail-recursive *)
val max : ('a -> 'a -> int) -> 'a list -> 'a

(** 0-based indexing. *)
val nth : int -> 'a list -> 'a

val cross_product : 'a list -> 'b list -> ('a * 'b) list
val cross_products : 'a list list -> 'a list list
val cross_product_map : ('a -> 'b -> 'c) -> 'a list -> 'b list -> 'c list

(** Returns the union of the two sets represented as lists avoiding repetitions (provided the original lists have no repetitions) *)
val union : 'a list -> 'a list -> 'a list
val intersect : 'a list -> 'a list -> 'a list

(** [filter_out f l] is equivalent to [List.filter (fun x -> not (f x)) l]. *)
val filter_out : ('a -> bool) -> 'a list -> 'a list

(** [is_prefix l1 l2] Returns [None] if [l1] is not a prefix of [l2] and returns [Some suffix] if [l2 = l1 @ suffix]. *)
val is_prefix : 'a list -> 'a list -> 'a list option

(** Assumes the list isn't empty. *)
val random_element : 'a list -> 'a

val memr : 'a list -> 'a -> bool

val count : ('a -> bool) -> 'a list -> int
