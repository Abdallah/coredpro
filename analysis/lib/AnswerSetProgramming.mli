val guided_verify: Ast.T.problem -> Ast.T.reduction -> Ast.T.problem -> string
val verify: Ast.T.problem -> Ast.T.reduction -> Ast.T.problem -> string
val solve: Ast.T.problem -> Ast.T.instance -> string
val check: Ast.T.problem -> Ast.T.instance -> Ast.T.solution -> string
val run: Ast.T.algorithm -> Ast.T.instance -> string
