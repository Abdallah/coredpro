type substitution = Ast.T.term Variable.Map.t
type t = substitution option

val empty : t
val empty_sub : substitution
val compose : t -> t -> t
val unify : Ast.T.term -> Ast.T.term -> t
val unify_sequence : Ast.T.term list -> Ast.T.term list -> t
val apply_term : substitution -> Ast.T.term -> Ast.T.term
val apply_atomic : substitution -> Ast.T.atomic -> Ast.T.atomic
val apply_literal : substitution -> Ast.T.literal -> Ast.T.literal
val add_equality : substitution -> Ast.T.term * Ast.T.term -> t
val equalities : t -> (string * Ast.T.term) list option
