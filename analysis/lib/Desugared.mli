val rename_variables_term : string Variable.Map.t -> Ast.T.term -> Ast.T.term
val rename_variables_atomic : string Variable.Map.t -> Ast.T.atomic -> Ast.T.atomic
val rename_variables_lit : string Variable.Map.t -> Ast.T.literal -> Ast.T.literal

val predicates : Ast.T.program -> Pred.Set.t
val symbols : Ast.T.program -> Symbol.Set.t
