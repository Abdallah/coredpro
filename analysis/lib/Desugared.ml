open Printf

let rename_var map var = Ast.T.Expr (Ast.T.Variable (Variable.Map.find_default var var map))
let rename_variables_term map term = Ast.map_variable_term (rename_var map) term
let rename_variables_atomic map term = Ast.map_variable_atomic (rename_var map) term
let rename_variables_lit map lit = Ast.map_variable_lit (rename_var map) lit

let predicates_literal : Ast.T.literal -> Pred.t option = function
  | Okay (pred, _) | Negation (pred, _) -> Some pred
  | Predefined _ -> None
let predicates_topliteral = function
  | Ast.T.Lit l -> predicates_literal l
let predicates_clause (head, body) =
  let (pred, _) = head in
  Pred.Set.add pred (Pred.Set.of_list (List.filter_map predicates_topliteral body))
let predicates prog = Pred.Set.maps_union predicates_clause prog

let rec symbols_term : Ast.T.term -> Symbol.Set.t = function
  | Ast.T.Fact (s, args) -> Symbol.Set.add s (Symbol.Set.maps_union symbols_term args)
  | Ast.T.Expr _ -> Symbol.Set.empty
let symbols_atom (_, args) = Symbol.Set.maps_union symbols_term args
let symbols_predefined = function
  | Ast.T.Unary (_, t) ->  symbols_term t
  | Ast.T.Relation (_, t1, t2) -> Symbol.Set.union (symbols_term t1) (symbols_term t2)
let symbols_literal : Ast.T.literal -> Symbol.Set.t = function
  | Okay atom | Negation atom -> symbols_atom atom
  | Predefined p -> symbols_predefined p
let symbols_topliteral = function
  | Ast.T.Lit l -> symbols_literal l
let symbols_clause (head, body) =
  let from_body = Symbol.Set.maps_union symbols_topliteral body in
  Symbol.Set.union (symbols_atom head) from_body
let symbols prog = Symbol.Set.maps_union symbols_clause prog
