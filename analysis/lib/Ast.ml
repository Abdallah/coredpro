open Printf

include Types.AST
open T


module Print =
struct

  let binop = function
    | Add -> "+"
    | Div -> "/"
    | Mult -> "*"
    | Log -> "//"
    | Mod -> "%"
    | Pow -> "**"
    | Sub -> "-"

  (*let symbol (f, _) = sprintf "%s" f*)
  let symbol (f, a) = sprintf "%s%d" f a
  let namespace = function
    | Input      -> "inp"
    | Output     -> "out"
    | Reduction  -> "red"
  let keyword : keyword -> string = function
    | Base -> "base"
    | Basis -> "basis"
    | Candidate -> "candidate"
    | Certificate -> "cert"
    | Constant s -> symbol s
    | Direct -> "direct"
    | Domain -> "domain"
    | Instance -> "instance"
    | Solution -> "solution"
  let unipred = function
    | First -> "first"
    | Last -> "last"
  let comparator = function
    | LessEq -> "le"
    | LessThan -> "lt"
    | MoreEq -> "ge"
    | MoreThan -> "gt"
    | Eq -> "eq"
    | Neq -> "neq"

  let predicate (n, k) = match n with
    | None -> sprintf "%s" (keyword k)
    | Some n -> sprintf "%s::%s" (namespace n) (keyword k)

  let variable v = "?" ^ v
  let rec expr = function
    | BinOp (bo, e1, e2) -> sprintf "%s %s %s" (inner_expr e1) (binop bo) (inner_expr e2)
    | Variable _ | Int _ as e -> inner_expr e
  and inner_expr = function
    | Variable n -> variable n
    | Int i -> Utils.Print.int i
    | BinOp _ as e -> sprintf "(%s)" (expr e)

  let rec term : term -> string = function
    | Fact (s, []) -> (symbol s)
    | Fact (s, l) -> sprintf "(%s %s)" (symbol s) (Utils.Print.unspaces term l)
    | Expr e -> expr e
  let atomic (s, l) = match l with
    | [] -> predicate s
    | _ -> sprintf "(%s %s)" (predicate s) (Utils.Print.unspaces term l)

  let predefined = function
    | Unary (a, t) -> sprintf "(%s %s)" (unipred a) (term t)
    | Relation (b, t1, t2) ->  sprintf "(%s %s %s)" (comparator b) (term t1) (term t2)

  let literal = function
    | Okay t -> atomic t
    | Negation t -> sprintf "(not %s)" (atomic t)
    | Predefined p -> predefined p

  let topliteral = function
    | Lit l -> literal l
  let rule (l, rs) =
    if rs = [] then atomic l
    else sprintf "(<= %s %s)" (atomic l) (Utils.Print.unspaces topliteral rs)

  let program prog = Utils.Print.unlines rule prog
end

let variable s = Expr (Variable s)
let arity (_, a) = a
let arity_kw = function
  | Base -> 2
  | Basis -> 1
  | Candidate -> 1
  | Certificate -> 0
  | Constant s -> arity s
  | Direct -> 0
  | Domain -> 1
  | Instance -> 1
  | Solution -> 1
let arity_pred (_, p) = arity_kw p

let variables_expr expr =
  let rec aux accu = function
    | Variable v -> v :: accu
    | BinOp (_, e1, e2) -> aux (aux accu e1) e2
    | Int _ -> accu in
  List.rev (aux [] expr)
let variables_term term =
  let rec aux : string list -> term -> string list = fun accu -> function
    | Fact (_, ts) -> List.fold_left aux accu ts
    | Expr e -> variables_expr e in
  aux [] term
let variables_atomic (_, args) = List.concat_map variables_term args
let variables_predefined = function
  | Unary (_, t) -> variables_term t
  | Relation (_, t1, t2) -> Utils.PList.uniques (variables_term t1 @ variables_term t2)

let variables_lit = function
  | Okay atom | Negation atom -> Utils.PList.uniques (variables_atomic atom)
  | Predefined p -> variables_predefined p

let variables_toplit = function
  | Lit l -> variables_lit l
let variables_rule (head, body) =
  let head_vars = variables_atomic head
  and body_vars = Utils.PList.uniques (List.concat_map variables_toplit body) in
  let unsafe_vars = List.filter (fun v -> not (List.mem v body_vars)) head_vars in
  if unsafe_vars <> [] then failwith (sprintf "Unsafe variable(s) %s in rule %s" (Utils.Print.unspaces Fun.id unsafe_vars) (Print.rule (head, body)));
  assert (List.for_all (Utils.PList.memr body_vars) head_vars);
  body_vars

let rec map_variable_term : (Variable.t -> term) -> term -> term = fun f -> function
  | Expr (Variable v) -> f v
  | Fact (symb, terms) -> Fact (symb, List.map (map_variable_term f) terms)
  | Expr e -> assert (not (variables_expr e = [])); Expr e
let map_variable_atomic f (pred, args) = (pred, List.map (map_variable_term f) args)
let map_variable_predefined f = function
  | Unary (a, t) -> Unary (a, map_variable_term f t)
  | Relation (a, t1, t2) -> Relation (a, map_variable_term f t1, map_variable_term f t2)
let map_variable_lit f = function
  | Okay atom -> Okay (map_variable_atomic f atom)
  | Negation atom -> Negation (map_variable_atomic f atom)
  | Predefined p -> Predefined (map_variable_predefined f p)
let map_variable_toplit f = function
  | Lit l -> Lit (map_variable_lit f l)
let map_pred_atom f (pred, args) = (f pred, args)
let map_pred_lit f = function
  | Okay atom -> Okay (map_pred_atom f atom)
  | Negation atom -> Negation (map_pred_atom f atom)
  | Predefined _ as r -> r
let map_pred_toplit f = function
  | Lit l -> Lit (map_pred_lit f l)
let map_pred_clause f (head, body) = (map_pred_atom f head, List.map (map_pred_toplit f) body)
let map_pred f = List.map (map_pred_clause f)


let map_atomic_lit : (atomic -> atomic) -> literal -> literal = fun f -> function
  | Okay atom -> Okay (f atom)
  | Negation atom -> Negation (f atom)
  | Predefined _ as p -> p
let map_atomic_toplit f = function
  | Lit l -> Lit (map_atomic_lit f l)
let map_atomic_clause f (head, body) = (f head, List.map (map_atomic_toplit f) body)
let map_atomic f = List.map (map_atomic_clause f)

let negate = function
  | Negation atom -> Okay atom
  | Okay atom -> Negation atom
  | Predefined _ -> assert false

let negate_top = function
  | Lit l -> Lit (negate l)

let special_keywords = [Certificate; Basis]

let add_namespace (n : T.namespace) = map_pred (fun (no, kw) -> assert (no = None); (Some n, kw))

