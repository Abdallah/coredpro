include module type of Types.COMBINED with module T = Types.COMBINED.T

val x : string

module GuidedVerify : sig
val input_base        : T.predicate
val direct            : T.atomic
val input_certificate : T.atomic
val ouput_certificate : T.atomic
val input_basis       : T.atomic
val input_domain      : T.atomic
val input_candidate   : T.atomic
val ouput_candidate   : T.atomic
val input_instance    : T.atomic
val ouput_instance    : T.atomic
val input_solution    : T.atomic
val ouput_solution    : T.atomic

val program : Ast.T.specification -> T.program
end

module Solve : sig
val candidate   : T.atomic
val solution    : T.atomic
val certificate : T.atomic

val program : Ast.T.problem -> Ast.T.program -> T.program
end

module Check : sig
val candidate   : T.atomic
val solution    : T.atomic
val certificate : T.atomic

val program : Ast.T.problem -> Ast.T.instance -> Ast.T.solution -> T.program
end

module Run : sig
(*val candidate   : T.atomic
val solution    : T.atomic
val certificate : T.atomic*)

val solution    : T.atomic
val program : Ast.T.algorithm -> Ast.T.instance -> T.program
end
