open Printf

module Opt = Optimization

let clean_name s =
  let reg = Str.regexp "-" in
  let s = Str.global_replace reg "_" s in
  let reg = Str.regexp "/" in
  Str.global_replace reg "_" s

type debug =
  { mutable parsed : bool;
    mutable desugared : bool;
    mutable optimized : bool }

type mode =
  | Certify of (bool * string * string)
  | Check of (string * string * string)
  | Reduce of (string * string)
  | Run of (string * string)
  | Solve of (string * string)
  | Synthesize of (bool * string * string)
  | Verify of (bool * bool * string * string * string)

let mode = ["certify"; "check"; "reduce"; "run"; "solve"; "synthesize"; "verify"]

(*type options =
  { input_names : string list;
    output_name : string;
    direction : Opt.proof_direction;
    asp : bool;
    verbose : int ref;
    debug : debug }

let print_debug d =
  sprintf "parsed:%b; desugared:%b; optimized:%b" d.parsed d.desugared d.optimized
let print { input_names; output_name; direction; asp; verbose; debug } =
  sprintf "input_name:%s;\noutput_name:%s;\ndirection:%s;\nasp:%b;\nverbose:%d;\n%s" (Utils.Print.list Utils.Print.string input_names) output_name (Opt.print_proof_direction direction) asp !verbose (print_debug debug)*)

let debug_true () =
  { parsed       = true; desugared    = true; optimized    = true }
let debug_false () =
  { parsed       = false; desugared    = false; optimized    = false }

let directions = ["both"; "implies"; "implied"]
let read_direction = function
    | "both"        -> Opt.Both
    | "implies"     -> Opt.Implies
    | "implied"     -> Opt.Implied
    | _ -> assert false

let add_defaults specs =
  let add_one (option, spec, descr, default) = (option, spec, sprintf "%s Default: %s" descr default) in
  List.map add_one specs

let skip_defaults specs =
  let skip_one (option, spec, descr, _) = (option, spec, descr) in
  List.map skip_one specs

let update reference value = reference := value

type options =
  { mode : string ref;
    algorithm : string ref;
    input : string ref;
    instance : string ref;
    output : string ref;
    problem : string ref;
    reduction : string ref;
    solution : string ref;
    bounded : bool ref;
    sol_map : bool ref;
  }

let verify opt speclist =
  let verify_speclist =
    [("--input",     Arg.Set_string opt.input,     "Specify the input problem file.");
     ("--reduction", Arg.Set_string opt.reduction, "Specify the reduction file.");
     ("--output",    Arg.Set_string opt.output,    "Specify the output problem file.");
     ("--bounded",   Arg.Set opt.bounded,  sprintf "Bounded verification. Default: %B" !(opt.bounded));
     ("--solution-mapping", Arg.Set opt.sol_map, sprintf "Use helpers for the solution mapping. Default: %B" !(opt.sol_map));] in
  let mode () = speclist := verify_speclist; opt.mode := "verify" in
  let gather names =
    let i,r,o = !(opt.input), !(opt.reduction), !(opt.output) in
    match names, List.mem "" [i;r;o] with
    | [], false -> Verify (!(opt.bounded), !(opt.sol_map), i, r, o)
    | _ :: _, _ | _, true -> failwith "Wrong number of arguments. Usage: coredpro --verify --input=file1 --reduction=file2 --output=file3." in
  (mode, gather)

let solve opt speclist =
  let solve_speclist =
    [("--problem", Arg.Set_string opt.problem, "Problem file.");
     ("--instance", Arg.Set_string opt.instance, "Instance file.");] in
  let mode () = speclist := solve_speclist; opt.mode := "solve" in
  let gather names =
    let p,i = !(opt.problem), !(opt.instance) in
    match names, List.mem "" [p;i] with
    | [], false -> Solve (p, i)
    | _ :: _, _ | _, true -> failwith "Wrong number of arguments. Usage: coredpro --solve --problem=file1 --instance=file2." in
  (mode, gather)

let check opt speclist =
  let check_speclist =
    [("--problem", Arg.Set_string opt.problem, "Problem file.");
     ("--instance", Arg.Set_string opt.instance, "Instance file.");
     ("--solution", Arg.Set_string opt.solution, "Solution file.");] in
  let mode () = speclist := check_speclist; opt.mode := "check" in
  let gather names =
    let p,i,s = !(opt.problem), !(opt.instance), !(opt.solution) in
    match names, List.mem "" [p;i;s] with
    | [], false -> Check (p, i, s)
    | _ :: _, _ | _, true -> failwith "Wrong number of arguments. Usage: coredpro --check --problem=file1 --instance=file2 --solution=file3." in
  (mode, gather)

let run opt speclist =
  let check_speclist =
    [("--algorithm", Arg.Set_string opt.algorithm, "Algorithm file.");
     ("--instance", Arg.Set_string opt.instance, "Instance file.");] in
  let mode () = speclist := check_speclist; opt.mode := "run" in
  let gather names =
    let a,i = !(opt.algorithm), !(opt.instance) in
    match names, List.mem "" [a;i] with
    | [], false -> Run (a, i)
    | _ :: _, _ | _, true -> failwith "Wrong number of arguments. Usage: coredpro --run --algorithm=file1 --instance=file2." in
  (mode, gather)

let get () =
  let speclist = ref [] in
  let opt = { mode = ref ""; algorithm = ref ""; input = ref ""; instance = ref ""; output = ref ""; problem = ref ""; reduction = ref ""; solution = ref ""; bounded = ref true; sol_map = ref true } in
  let verify_mode, verify_gather = verify opt speclist in
  let solve_mode, solve_gather = solve opt speclist in
  let check_mode, check_gather = check opt speclist in
  let run_mode, run_gather = run opt speclist in
  let start_speclist =
    [("--verify", Arg.Unit verify_mode, "Verify mode.");
     ("--solve", Arg.Unit solve_mode, "Solve mode.");
     ("--check", Arg.Unit check_mode, "Check mode.");
     ("--run", Arg.Unit run_mode, "Run mode.")] in
  speclist := start_speclist;
  let input_names = ref [] in
  let add_name s = input_names := s :: !input_names in
  let usage_msg = "CoredPro. Options available:" in
  Arg.parse_dynamic speclist add_name usage_msg;
  let gather = match !(opt.mode) with
    | "verify" -> verify_gather
    | "solve" -> solve_gather
    | "check" -> check_gather
    | "run" -> run_gather
    | _ -> failwith (sprintf "Not implemented %s." !(opt.mode)) in
  gather !input_names
(*let get () =
  let debug = debug_false () in
  let verbose = ref 0 in
  let input_names = ref [] in
  let output_name = ref "" in
  let direction = ref Opt.Both in
  let set_direction direc = direction := read_direction direc in
  let speclist =
    [("--verbose",        Arg.Set_int verbose, "Enable verbose mode.", sprintf "%d" !verbose);
     ("-o",               Arg.Set_string output_name, "Set the output path.", !output_name);
     ("--direction",      Arg.Symbol (directions, set_direction), "Set the proof direction.", sprintf "%s" (Opt.print_proof_direction !direction));
     ("-",                Arg.Unit (fun () -> update input_names ("-" :: !input_names)), "Read the GDL from the standard input.", "Disabled");
    ] in
  let usage_msg = "CoredPro. Options available:" in
  let add_name s = input_names := s :: !input_names in
  Arg.parse (add_defaults speclist) add_name usage_msg;
  let options =
    { input_names = List.rev !input_names;
      output_name = !output_name;
      direction = !direction;
      asp = !asp;
      verbose;
      debug } in
  if List.length (!input_names) <> 3 then failwith "Not enough arguments. Usage: coredpro reduction problem1 problem2";
  options
*)
