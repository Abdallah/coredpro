open Printf

type proof_direction = Both | Implies | Implied

let print_proof_direction = function
  | Both -> "Both"
  | Implies -> "Implies"
  | Implied -> "Implied"

let optimize name switch f prog =
  if switch then
    (eprintf "%s %!" name;
     let (prog, time) = Utils.Time.measure f prog in
     eprintf "%f\n%!" time;
     prog)
  else prog

let make param prog =
(*  let prog = optimize "compute rigids" !(param.compute_rigids) ComputeRigids.get prog in
  let prog = optimize "compute rigids" (!(param.compute_rigids) && !(param.merge_rigids)) ComputeRigids.get prog in*)
  let prog = optimize "inline" (param >= 0) (Inlining.program param) prog in
  prog
