open Printf

let deprintf format = if true then eprintf format else ifprintf stderr format

module DS = Variable.DisjointSet

let rename_lit map = Unification.apply_literal (Variable.Map.map Ast.variable map)
let rename_toplit map = function
  | Ast.T.Lit l -> Ast.T.Lit (rename_lit map l)
let make_eqs vars args =
  assert (List.length vars = List.length args);
  let var_args = List.combine vars args in
  let rec aux : Ast.T.literal list * string Variable.Map.t -> string * Ast.T.term -> Ast.T.literal list * string Variable.Map.t = fun (accu_eqs, accu_sub) (var, term) ->
    match Desugared.rename_variables_term accu_sub term with
    | Ast.T.Expr (Ast.T.Variable v) when not (List.mem v vars) -> (accu_eqs, Variable.Map.add v var accu_sub)
    | term2 -> (Ast.T.Predefined (Ast.T.Relation (Ast.T.Eq, Ast.T.Expr (Ast.T.Variable var), term2)) :: accu_eqs, accu_sub) in
  List.fold_left aux ([], Variable.Map.empty) var_args

let force_body vars args body =
  let (eqs, sub) = make_eqs vars args in
  let eqs = List.map (fun l -> Ast.T.Lit l) eqs in
  Utils.PList.map (rename_toplit sub) body @ eqs

(** Collect the different predicates and map each one to a pair (list of variables * DNF) that can generate it. *)
let regroup program =
  let make_vars pred = Utils.PList.map (sprintf "inline_%d") (Utils.PList.range 0 (Ast.arity_pred pred)) in
  let add map ((pred, args), body) =
    let (vars, bodies) = Pred.Map.find_default pred (make_vars pred, []) map in
    let body = force_body vars args body in
    Pred.Map.add pred (vars, body :: bodies) map in
  List.fold_left add Pred.Map.empty program

let add_literal context disj_set (var_map, others) lit =
  let relevant_vars = Utils.PList.subtract (Ast.variables_toplit lit) context in
  match relevant_vars with
  | [] -> (var_map, lit :: others)
  | var :: rest ->
    let var = DS.find var disj_set in
    assert (List.for_all (fun elt -> DS.find elt disj_set = var) rest);
    (Variable.Map.update var (List.cons lit) [] var_map, others)

let new_predicate = Generate.predicate (Some Ast.T.Input) "inline_negation"

let make_literal context var lits (accu, new_rules) =
  assert (List.length lits > 0);
  let body_vars = List.concat_map Ast.variables_toplit lits in
  let comm_vars = List.filter (Utils.PList.memr body_vars) context in
(*  let new_rules, new_head = Generate.add_rule_ast new_rules comm_vars lits in*)
  let new_head = (new_predicate (List.length comm_vars), List.map Ast.variable comm_vars) in
  deprintf "Inline: %s %s\n" (Ast.Print.atomic new_head) (Utils.Print.list Ast.Print.topliteral lits);
  (Ast.T.Lit (Ast.T.Okay new_head) :: accu, (new_head, lits) :: new_rules)
(*    (Ast.T.Okay new_head :: accu, new_rules)*)

let cluster_variables context (accu, new_rules) conj =
  let relevant_vars lit = Utils.PList.subtract (Ast.variables_toplit lit) context in
  let use_literal dset lit = DS.unions (relevant_vars lit) dset in
  let disj_set = List.fold_left use_literal DS.empty conj in
  let (var_map, others) = List.fold_left (add_literal context disj_set) (Variable.Map.empty, []) conj in
  let lits, ruls = Variable.Map.fold (make_literal context) var_map ([], new_rules) in
  ((lits @ others) :: accu, ruls)

let inline_neg new_rules args vars bodies =
  eprintf "new_rules = %s\nargs = %s\nvars = %s\nbodies = %s\n%!" (Utils.Print.list (Utils.Print.couple Ast.Print.atomic (Utils.Print.unspaces Ast.Print.topliteral)) new_rules) (Utils.Print.list Ast.Print.term args) (Utils.Print.list Variable.print vars) (Utils.Print.unspaces (Utils.Print.list Ast.Print.topliteral) bodies);
  let original_vars = Utils.PList.uniques (List.concat_map Ast.variables_term args) in
  assert (List.length args = List.length vars);
  assert (Utils.PList.no_doubles vars);
  let var_args = List.combine vars args in
  let var_map = Variable.Map.from_bindings var_args in
  let replace var = Variable.Map.find_default var (Ast.T.Expr (Ast.T.Variable var)) var_map in
  let bodies = Utils.PList.map (Utils.PList.map (Ast.map_variable_toplit replace)) bodies in
  let bodies, rules = List.fold_left (cluster_variables original_vars) ([], new_rules) bodies in
  let bodies = Utils.PList.map (Utils.PList.map Ast.negate_top) bodies in
  eprintf "hola5 %d %s\n%!" (List.length bodies) (Utils.Print.list (fun l -> Utils.Print.int (List.length l)) bodies);
  eprintf "%s\n%!" (Utils.Print.unlines (Utils.Print.list Ast.Print.topliteral) bodies);
  let bodies = Utils.PList.cross_products bodies in
  eprintf "hola6\n%!";
  (bodies, rules)

let inline_pos args vars bodies =
  assert (List.length args = List.length vars);
  let eqs = List.map2 (fun var arg -> Ast.T.Lit (Ast.T.Predefined (Ast.T.Relation (Ast.T.Eq, Ast.T.Expr (Ast.T.Variable var), arg)))) vars args in
  List.map ((@) eqs) bodies

let inline_literal_aux inputs group new_rules = function
  | Ast.T.Predefined _ -> assert false
  | Ast.T.Negation (pred, _) | Ast.T.Okay (pred, _) when List.mem pred inputs -> None
  | Ast.T.Okay (pred, args) ->
    assert (Pred.Map.mem pred group);
    let (vars, bodies) = Pred.Map.find pred group in
    Some (inline_pos args vars bodies, new_rules)
  | Ast.T.Negation (pred, args) ->
    let (vars, bodies) = Pred.Map.find pred group in
    Some (inline_neg new_rules args vars bodies)

let inline_topliteral_aux inputs group new_rules = function
  | Ast.T.Lit l -> inline_literal_aux inputs group new_rules l
let inline_topliteral inputs param group (accu_lits, new_rules) lit = match inline_topliteral_aux inputs group new_rules lit with
  | None -> ([[lit]] :: accu_lits, new_rules)
  | Some (lits, _) when List.length lits > param -> ([[lit]] :: accu_lits, new_rules)
  | Some (lits, rules) -> (lits :: accu_lits, rules)

(*let inline_topliteral inputs param group (accu_lits, new_rules) = function
  | Ast.T.Lit l -> inline_literal inputs param group (accu_lits, new_rules) l*)

let is_consistent (_, body) =
  let get_lit = function
    | Ast.T.Lit l -> Some l in
  let body = List.filter_map get_lit body in
  let is_fact : Ast.T.literal -> (Ast.T.atomic, Ast.T.atomic) Either.t option = function
    | Ast.T.Predefined _ -> None
    | Ast.T.Okay fact -> Some (Either.Right fact)
    | Ast.T.Negation fact -> Some (Either.Left fact) in
  let negs, poss = List.partition_map Fun.id (List.filter_map is_fact body) in
  Utils.PList.intersect negs poss = []

let inline_clause inputs param group (accu, new_rules) (head, body) =
  eprintf "cou0 %s %d %d\n%!" (Ast.Print.rule (head, body)) (List.length accu) (List.length new_rules);
  let inlined_lits, rules = List.fold_left (inline_topliteral inputs param group) ([], new_rules) body in
  eprintf "cou1\n%!";
  let inlined_lits = List.rev inlined_lits in
  eprintf "cou2\n%!";
  let bodies = Utils.PList.cross_products inlined_lits in
  eprintf "cou3\n%!";
  let clauses = Utils.PList.uniques (Utils.PList.map (fun bod -> (head, Utils.PList.uniques (Utils.PList.flatten bod))) bodies) in
  eprintf "cou4\n%!";
  let clauses = Utils.PList.uniques (List.filter is_consistent clauses) in
  eprintf "cou5\n%!";
  List.rev_append clauses accu, rules

let inline_aux param base prog =
  let group = regroup base in
  eprintf "group:\n%s\n%!" (Pred.Map.print (Utils.Print.couple (Utils.Print.list Utils.Print.string) (Utils.Print.list (Utils.Print.list Ast.Print.topliteral))) group);
  let inputs = assert false in
(*  let rules, new_rules = List.fold_left (inline_clause param group) ([], Generate.empty new_predicate) prog in*)
  let rules, new_rules = List.fold_left (inline_clause inputs param group) ([], []) prog in
  List.rev_append rules new_rules (*Generate.get new_rules*)

let inline_program param prog = inline_aux param prog prog

let program param prog = if param < 0 then prog else inline_program param prog

(** Things to do: avoid recreating existing rules so that we can use a fixpoint computation. *)
(*
module Ground = struct
  module Term = Utils.DataStructure.F (struct type t = Ast.ground Ast.term let compare = compare let print = Ast.Print.term end)
  module GAtomic = Utils.DataStructure.F (struct type t = (Ast.predicate, Ast.ground) Ast.atomic let compare = compare let print = Ast.Print.atomic end)

  let regroup program =
    let add map clause =
      let (head, body) = clause in
      assert (Desugared.variables_clause clause = []);
      GAtomic.Map.update head (Utils.PList.cons body) [] map in
    List.fold_left add GAtomic.Map.empty program

  let inline_pos (param1, param2) group atom = match atom with
    | (pred, _) when List.mem pred inputs -> [[Desugared.Okay atom]]
    | _ -> let substitute = GAtomic.Map.find_default atom [] group in
           let width = List.length substitute
           and length = Utils.PList.max compare (0 :: Utils.PList.map List.length substitute) in
           if width > param1 || length > param2 then [[Desugared.Okay atom]] else substitute

  let negate : ('a, Ast.ground) Desugared.literal -> ('a, Ast.ground) Desugared.literal = function
    | Desugared.Okay atom -> Desugared.Negation atom
    | Desugared.Negation atom -> Desugared.Okay atom
    | Desugared.Abstract _ -> assert false

  let inline_neg (param1, param2) group atom = match atom with
    | (pred, _) when List.mem pred inputs -> [[Desugared.Negation atom]]
    | _ -> let substitute = GAtomic.Map.find_default atom [] group in
           let width = List.fold_left (fun x y -> x * List.length y) 1 substitute
           and length = List.length substitute in
           if width > param1 || length > param2 then [[Desugared.Negation atom]]
           else
             let substitute = Utils.PList.cross_products substitute in
             Utils.PList.map (Utils.PList.map negate) substitute
  let inline_lit param group : ('a, Ast.ground) Desugared.literal -> ('a, Ast.ground) Desugared.literal list list = function
    | Desugared.Okay atomic -> inline_pos param group atomic
    | Desugared.Negation atomic -> inline_neg param group atomic
    | Desugared.Abstract _ -> assert false

  let inline_rule param (group : (('a, Ast.ground) Desugared.literal list list) GAtomic.Map.t) (head, body) =
    let bodies = Utils.PList.map (inline_lit param group) body in
    let bodies = Utils.PList.cross_products bodies in
    Utils.PList.map (fun body -> (head, Utils.PList.flatten body)) bodies

  let rec program_aux param prog =
    let group = regroup prog in
    let prog' = Utils.PList.flatten (Utils.PList.map (inline_rule param group) prog) in
    if prog' <> prog then program_aux param prog' else prog'

  let rec program param prog =
    let prog' = program_aux param prog in
    Reachability.make Reachability.Graph prog'
end
*)
