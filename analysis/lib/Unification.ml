open Printf

type substitution = Ast.T.term Variable.Map.t
type t = substitution option

let print_sub x = Variable.Map.print Ast.Print.term x
let print x = Utils.Print.option print_sub x

let empty_sub = Variable.Map.empty
let empty = Some empty_sub

let apply_sub sub var = Variable.Map.find_default var (Ast.T.Expr (Ast.T.Variable var)) sub
let apply_term sub = Ast.map_variable_term (apply_sub sub)
let apply_atomic sub = Ast.map_variable_atomic (apply_sub sub)
let apply_predefined sub = Ast.map_variable_predefined (apply_sub sub)
let apply_literal sub = Ast.map_variable_lit (apply_sub sub)
let equalities x = Option.map Variable.Map.bindings x

let codomains sub =
  let dom, terms = Utils.PList.split (Variable.Map.bindings sub) in
  let free_vars = List.concat_map Ast.variables_term terms in
  let cod = Utils.PList.uniques free_vars in
  assert (Utils.PList.intersect dom cod = []);
  assert (Utils.PList.no_doubles dom);
  dom, cod

let compose_aux tau theta =
  let dom1, cod1 = codomains tau
  and dom2, cod2 = codomains theta in
  if (Utils.PList.intersect dom1 dom2 <> []) then eprintf "Unification:%s %s\n" (print_sub tau) (print_sub theta);
  assert (Utils.PList.intersect dom1 dom2 = []);
  assert (Utils.PList.intersect dom1 cod2 = []);
  let add_one key term accu = Variable.Map.add key (apply_term theta term) accu in
  Variable.Map.fold add_one tau theta
let compose xo yo = Option.bind xo (fun x -> Option.bind yo (fun y -> Some (compose_aux x y)))

let rec unify_sequence_aux : Ast.T.term list -> Ast.T.term list -> t = fun l1 l2 ->
  assert (List.length l1 = List.length l2);
  let aux t1 t2 = function
    | None -> None
    | Some tau ->
      let theta = unify (apply_term tau t1) (apply_term tau t2) in
      Option.map (compose_aux tau) theta in
  List.fold_right2 aux l1 l2 empty
and unify : Ast.T.term -> Ast.T.term -> t = fun term1 term2 -> match term1, term2 with
  | Ast.T.Expr (Ast.T.Variable v1), Ast.T.Expr (Ast.T.Variable v2) when (v1 = v2) -> empty
  | Ast.T.Expr (Ast.T.Variable var), term
  | term, Ast.T.Expr (Ast.T.Variable var) -> if List.mem var (Ast.variables_term term) then None else Some (Variable.Map.singleton var term)
  | Ast.T.Fact (f1,  _), Ast.T.Fact (f2,  _) when f1 <> f2 -> None
  | Ast.T.Fact (f1, l1), Ast.T.Fact (f2, l2) ->
     assert (f1 = f2 && List.length l1 = List.length l2);
     let u = unify_sequence_aux l1 l2 in u
  | Ast.T.Expr _, _ | _, Ast.T.Expr _ -> assert false

let unify_sequence l1 l2 =
  assert (List.length l1 = List.length l2);
  unify_sequence_aux l1 l2

let add_equality sub (t1, t2) =
  let term1 = apply_term sub t1
  and term2 = apply_term sub t2 in
  compose (Some sub) (unify term1 term2)
