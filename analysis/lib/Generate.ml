open Printf

let deprintf format = if false then eprintf format else ifprintf stderr format

module Hyps =
struct
  module Ord = struct type t = string list * Ast.T.topliteral list
                      let compare = compare
                      let print = Utils.Print.couple (Utils.Print.list Utils.Print.string) (Utils.Print.unspaces Ast.Print.topliteral) end
  include Utils.DataStructure.F (Ord)
end

type 'a t = (int -> 'a) * Ast.T.atomic Hyps.Map.t

(*let make_head predicate required_vars lits =
  let vars = List.concat_map Ast.variables_lit lits in
  assert (List.for_all (Utils.PList.memr vars) required_vars);
  let required_vars = List.sort compare required_vars in
  let pred = predicate (List.length required_vars) in
  (pred, Utils.PList.map Ast.variable required_vars)

let replace_vars map args = Utils.PList.map (Desugared.rename_variables_term map) args

let choose_one =
  let rec aux choices before = function
    | [] -> List.rev choices
    | a :: after -> aux ((a, List.rev_append before after) :: choices) (a :: before) after in
  aux [] []

let rec unify_args map b1 b2 = match b1, b2 with
  | [], [] -> Some map
  | [], _ :: _ | _ :: _, [] -> assert false
  | Ast.T.Fact a1 :: r1, Ast.T.Fact (s2, a2) :: r2 -> if a1 = (s2, replace_vars map a2) then unify_args map r1 r2 else None
  | Ast.T.Fact _ :: _, Ast.T.Expr (Ast.T.Variable _) :: _ | Ast.T.Expr (Ast.T.Variable _) :: _, Ast.T.Fact _ :: _ -> None
  | Ast.T.Expr (Ast.T.Variable v1) :: r1, Ast.T.Expr (Ast.T.Variable v2) :: r2 ->
    if Variable.Map.mem v2 map
    then if v1 = Variable.Map.find v2 map then unify_args map r1 r2 else None
    else if List.mem v1 (Variable.Map.values map) then None
    else unify_args (Variable.Map.add v2 v1 map) r1 r2
  | Ast.T.Expr _ :: _, _ | _, Ast.T.Expr _ :: _ -> assert false

let rec unify_body b1 b2 map = match b1, b2 with
  | [], [] -> [map]
  | [], _ :: _ -> []
  | a :: r, _ -> Utils.PList.filter_maps (unify_hyp map (a, r)) (choose_one b2)
and unify_predefined map p1 b1 p2 b2 = match p1, p2 with
  | Ast.T.Relation (p1, _, _), Ast.T.Relation (p2, _, _) when p1 <> p2 -> None
  | Ast.T.Relation (_, t1, t1'),   Ast.T.Relation (_, t2, t2') -> assert false; Option.map (unify_body b1 b2) (unify_args map [t1; t1'] [t2; t2'])
  | _, _ -> assert false
and unify_hyp map (a1, b1) (a2, b2) = match a1, a2 with
  | Ast.T.Okay (p1, _), Ast.T.Okay (p2, _)
  | Ast.T.Negation (p1, _), Ast.T.Negation (p2, _) when p1 <> p2 -> None
  | Ast.T.Predefined p1, Ast.T.Predefined p2 -> unify_predefined map p1 b1 p2 b2
  | Ast.T.Okay _, Ast.T.Negation _ | Ast.T.Okay _, Ast.T.Predefined _
  | Ast.T.Negation _, Ast.T.Okay _ | Ast.T.Negation _, Ast.T.Predefined _
  | Ast.T.Predefined _, Ast.T.Okay _ | Ast.T.Predefined _, Ast.T.Negation _ -> None
  | Ast.T.Negation (_, args1), Ast.T.Negation (_, args2)
  | Ast.T.Okay     (_, args1), Ast.T.Okay     (_, args2) -> Option.map (unify_body b1 b2) (unify_args map args1 args2)
(*  | Ast.T.Distinct (v1, t1),   Ast.T.Distinct (v2, t2)   -> Utils.Option.map (unify_body b1 b2) (unify_args map [Ast.T.Variable v1; t1] [Ast.T.Variable v2; t2])*)

let unify vars1 body1 (vars2, body2) head =
  assert (Utils.PList.no_doubles vars1 && Utils.PList.no_doubles vars2);
  if List.length vars1 <> List.length vars2 then None
  else
    let vars = List.combine vars2 vars1 in
    let map = Variable.Map.from_bindings vars in
    match unify_body body1 body2 map with
    | [] -> None
    | a :: r ->
      if r <> [] then eprintf "Generate many unifs: %d %s\n" (1 + List.length r) (Utils.Print.unspaces (Variable.Map.print Utils.Print.string) (a :: r));
      Some (Desugared.rename_variables_atomic a head)

let add_rule (predicates, rules) vars body =
  let substitutions = Hyps.Map.filter_map (unify vars body) rules in
  match Hyps.Map.values substitutions with
  | [] ->
    let head = make_head predicates vars body in
    deprintf "Generate created rule:\n%s\n%!" (Ast.Print.rule (head, body));
    ((predicates, Hyps.Map.add (vars, body) head rules), head)
  | head :: [] -> ((predicates, rules), head)
  | _ :: _ :: _ -> assert false
*)

let empty predicates = (predicates, Hyps.Map.empty)
let get (_, rules) = Hyps.Map.fold (fun (_, body) head accu -> (head, body) :: accu) rules []

let variable prefix =
  let counter = ref (-1) in
  let next () =
    incr counter;
    sprintf "%s%d" prefix !counter in
  next

let predicate namespace prefix =
  let counter = ref (-1) in
  let next arity =
    incr counter;
    let name = sprintf "%s%d" prefix !counter in
    (namespace, Ast.T.Constant (name, arity)) in
  next
