open Printf

type tfftype =  Bool | Individual | Int
type tvariable = Variable.t * tfftype
type maybeint = (Variable.t, int) Either.t

type atom_typing = Ast.T.predicate * tfftype list * tfftype

type t =
  | Direct
  | Conj of t list
  | Disj of t list
  | Atom of Ast.T.atomic
  | Exis of (tvariable list * t)
  | Fora of (tvariable list * t)
  | Nega of t
  | Predef of Ast.T.predefined
  | Dist of (Ast.T.term * Ast.T.term)
  | Equa of (Ast.T.term * Ast.T.term)
  | Less of maybeint * maybeint
  | Lessp of (Ast.T.atomic * Ast.T.atomic)
  | Impl of (t * t)
  | Equi of (t * t)

type role = Axiom | Conjecture | Type

type formula = FLogic of t | FType of atom_typing

type axiom =
  { name : string;
    role : role;
    body : formula; }

type program = axiom list

module Print =
struct
  let sort = function
    | Axiom -> "axiom"
    | Conjecture -> "conjecture"
    | Type -> "type"
  let escape = Utils.MString.escape ['-'; '+'; '<'; '>'] '_'
  let symbol symb = escape (Ast.Print.symbol symb)
(*  let predefined = function
    |
*)
  let predicate (n, k) = match n with
    | None -> escape (Ast.Print.keyword k)
    | Some n -> sprintf "%s_%s" (escape (Ast.Print.namespace n)) (escape (Ast.Print.keyword k))
(*    | Ast.T.Abstract a -> escape (Ast.Print.abstract a)*)
  let variable v =
    if Utils.MString.starts_lowercase v
    then escape (String.capitalize_ascii v)
    else escape (sprintf "V%s" v)
  let tfftype = function
    | Bool -> "$o"
    | Int -> "$int"
    | Individual -> "$i"
  let atom_typing (p, l, t) = match l with
    | [] -> sprintf "%s : %s" (predicate p) (tfftype t)
    | _ :: _ -> sprintf "%s : %s > %s" (predicate p) (Utils.Print.list' "" " * " "" tfftype l) (tfftype t)
  let tvariable (v, t) = sprintf "%s:%s" (variable v) (tfftype t)
  let rec term : Ast.T.term -> string = function
    | Ast.T.Expr (Ast.T.Variable v) -> variable v
    | Ast.T.Fact (s, []) -> symbol s
    | Ast.T.Fact (s, args) -> sprintf "%s%s" (symbol s) (Utils.Print.list' "(" "," ")" term args)
    | Ast.T.Expr _ ->  assert false
  let atom = function
    | (pred, []) -> predicate pred
    | (pred, args) -> sprintf "%s%s" (predicate pred) (Utils.Print.list' "(" "," ")" term args)
  let maybeint = Utils.Print.either variable Utils.Print.int
  let comparator = function
    | Ast.T.Eq -> "="
    | Ast.T.Neq -> "!="
    | Ast.T.LessEq -> assert false; "<="
    | Ast.T.LessThan -> "<"
    | Ast.T.MoreEq -> ">="
    | Ast.T.MoreThan -> ">"
  let predefined = function
    | Ast.T.Relation (b, t1, t2) -> sprintf "%s %s %s" (term t1) (comparator b) (term t2)
    | Ast.T.Unary _ -> assert false
  let rec t = function
    | Direct -> "direct"
    | Conj [] -> "$true"
    | Conj (a :: []) | Disj (a :: []) -> t a
    | Conj l -> Utils.Print.list' "(" "&" ")" t l
    | Disj [] -> "$false"
    | Disj l -> Utils.Print.list' "(" "|" ")" t l
    | Atom a -> atom a
    | Exis ([], a) | Fora ([], a) -> t a
    | Exis (vs, a) -> sprintf "(%s: %s)" (Utils.Print.list' "?[" ", " "]" tvariable vs) (t a)
    | Fora (vs, a) -> sprintf "(%s: %s)" (Utils.Print.list' "![" ", " "]" tvariable vs) (t a)
    | Nega (Atom a) -> sprintf "~%s" (atom a)
    | Nega a -> sprintf "~(%s)" (t a)
    | Predef p -> sprintf "(%s)" (predefined p)
    | Dist (t1, t2) -> sprintf "(%s!=%s)" (term t1) (term t2)
    | Equa (t1, t2) -> sprintf "(%s=%s)" (term t1) (term t2)
    | Less (v1, v2) -> sprintf "$less(%s, %s)" (maybeint v1) (maybeint v2)
    | Lessp (a1, a2) -> sprintf "$less(%s, %s)" (atom a1) (atom a2)
    | Impl (a1, a2) -> sprintf "(%s => %s)" (t a1) (t a2)
    | Equi (a1, a2) -> sprintf "(%s <=> %s)" (t a1) (t a2)
  let rec t_top = function
    | Direct -> "direct"
    | Conj [] -> "$true"
    | Conj (a :: []) | Disj (a :: []) -> t_top a
    | Conj l -> Utils.Print.list' "" "&" "" t l
    | Disj [] -> "$false"
    | Disj l -> Utils.Print.list' "" "|" "" t l
    | Atom a -> atom a
    | Exis ([], a) | Fora ([], a) -> t_top a
    | Exis (vs, a) -> sprintf "%s: %s" (Utils.Print.list' "?[" ", " "]" tvariable vs) (t a)
    | Fora (vs, a) -> sprintf "%s: %s" (Utils.Print.list' "![" ", " "]" tvariable vs) (t a)
    | Nega (Atom a) -> sprintf "~%s" (atom a)
    | Nega a -> sprintf "~(%s)" (t a)
    | Predef p -> sprintf "%s" (predefined p)
    | Dist (t1, t2) -> sprintf "%s!=%s" (term t1) (term t2)
    | Equa (t1, t2) -> sprintf "%s=%s" (term t1) (term t2)
    | Less (v1, v2) -> sprintf "$less(%s, %s)" (maybeint v1) (maybeint v2)
    | Lessp (a1, a2) -> sprintf "$less(%s, %s)" (atom a1) (atom a2)
    | Impl (a1, a2) -> sprintf "%s => %s" (t a1) (t a2)
    | Equi (a1, a2) -> sprintf "%s <=> %s" (t a1) (t a2)
  let formula = function
    | FLogic log -> t_top log
    | FType ts -> atom_typing ts
  let axiom { name; role; body } =
    sprintf "tff(%s,%s, %s)." name (sort role) (formula body)
  let program prog = Utils.Print.unlines axiom prog
end

let rec map_predicate f = function
  | Direct -> Direct
  | Conj l -> Conj (List.map (map_predicate f) l)
  | Disj l -> Disj (List.map (map_predicate f) l)
  | Atom (pred, args) -> Atom (f pred, args)
  | Lessp ((pred1, args1), (pred2, args2)) -> Lessp ((f pred1, args1), (f pred2, args2))
  | Exis (vs, a) -> Exis (vs, map_predicate f a)
  | Fora (vs, a) -> Fora (vs, map_predicate f a)
  | Nega a -> Nega (map_predicate f a)
  | Predef _ as p -> p
  | Dist _ | Equa _ | Less _ as a -> a
  | Impl (a1, a2) -> Impl (map_predicate f a1, map_predicate f a2)
  | Equi (a1, a2) -> Equi (map_predicate f a1, map_predicate f a2)
