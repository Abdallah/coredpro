val algorithm : string -> Ast.T.algorithm
val instance : string -> Ast.T.instance
val solution : string -> Ast.T.solution
val problem : string -> Ast.T.problem
val reduction : string -> Ast.T.reduction
