module AST = struct module T = struct
type symbol = string * int
type namespace = Input | Output | Reduction
type keyword = Base | Basis | Candidate | Certificate | Constant of symbol | Direct | Domain | Instance | Solution
type predicate = namespace option * keyword

type binop = Add | Div | Log | Mod | Mult | Pow | Sub
type expr = Int of int | Variable of Variable.t | BinOp of (binop * expr * expr) (*| UnOp of (unaryop * expr)*)
type term = Fact of (symbol * term list) | Expr of expr

type atomic = predicate * term list
type unipred = First | Last
type comparator = LessEq | LessThan | MoreEq | MoreThan | Neq | Eq
type predefined = Unary of (unipred * term) | Relation of (comparator * term * term)
type literal = Okay of atomic | Negation of atomic | Predefined of predefined
type aggop = Sum | Prod | Max | Min
type topliteral = Lit of literal (*| Aggregate of (Variable.t * aggop * term * term list * literal list)*)
type rule = atomic * topliteral list
type program = rule list

type instance = program
type solution = program
type algorithm = program

type reduction =
  { mapping : program;
    implies : program;
    implied : program }
type problem =
  { vocabulary    : program;
    solutioncheck : program }

type specification =
  { input : problem;
    reduction : reduction;
    output : problem }
end end

module COMBINED = struct module T = struct
type predicate = AST.T.symbol
type atomic = predicate * AST.T.term list
type literal = Okay of atomic | Negation of atomic | Predefined of AST.T.predefined
type topliteral = Lit of literal (*| Aggregate of (Variable.t * aggop * term * term list * literal list)*)
type rule = atomic * topliteral list
type program = rule list
end end
