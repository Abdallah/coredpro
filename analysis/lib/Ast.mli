include module type of Types.AST with module T = Types.AST.T

module Print :
sig
  val symbol : T.symbol -> string
  val namespace : T.namespace -> string
  val unipred : T.unipred -> string
  val comparator : T.comparator -> string
  val keyword : T.keyword -> string
  val predicate : T.predicate -> string
  val term : T.term -> string
  val atomic : T.atomic -> string
  val literal : T.literal -> string
  val topliteral : T.topliteral -> string
  val rule : T.rule  -> string
  val program : T.program -> string
end

val variable : Variable.t -> T.term
val arity : T.symbol -> int
val arity_kw : T.keyword -> int
val arity_pred : T.predicate -> int

val map_variable_term : (Variable.t -> T.term) -> T.term -> T.term
val map_variable_atomic : (Variable.t -> T.term) -> T.atomic -> T.atomic
val map_variable_predefined : (Variable.t -> T.term) -> T.predefined -> T.predefined
val map_variable_lit : (Variable.t -> T.term) -> T.literal -> T.literal
val map_variable_toplit : (Variable.t -> T.term) -> T.topliteral -> T.topliteral

val map_pred : (T.predicate -> T.predicate) -> T.program -> T.program
val map_atomic : (T.atomic -> T.atomic) -> T.program -> T.program

val variables_term : T.term -> Variable.t list
val variables_atomic : T.atomic -> Variable.t list
val variables_lit : T.literal -> Variable.t list
val variables_toplit : T.topliteral -> Variable.t list
val variables_rule : T.rule -> Variable.t list



val negate : T.literal -> T.literal
val negate_top : T.topliteral -> T.topliteral

val add_namespace : T.namespace -> T.program -> T.program
