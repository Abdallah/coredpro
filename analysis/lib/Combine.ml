open Printf

include Types.COMBINED
open T

(*let do_types spec =
  assert false;
(*  let input_types = Ast.add_namespace_types Ast.T.Input Ast.T.(spec.input.types)
  and output_types = Ast.add_namespace_types Ast.T.Output Ast.T.(spec.output.types) in
  let input_types = Types.compute (Utils.PList.flatten Ast.T.[input_types; output_types; spec.reduction.types]) in
  eprintf "input relations:\n%s\n" (Types.print input_types);*)
  ()
  (*input_types, output_types*)

let do_types2 spec =
  let inp_types = Ast.add_namespace Ast.T.Input Ast.T.(spec.input.types)
  and out_types = Ast.add_namespace Ast.T.Output Ast.T.(spec.output.types)
  and red_types = Ast.T.(spec.reduction.types) in
  let map_okay (head, body) = (head, List.map (fun at -> Ast.T.Okay at) body) in
  let input_types, reduction_types, output_types = inp_types, List.map map_okay red_types, out_types in
(*  let output_types = Types.compute output_types in
  eprintf "output relations:\n%s\n" (Types.print output_types);*)
  (input_types, reduction_types, output_types)
*)

let merge_namespace (n, p) =
  let a = Ast.arity_kw p in
  let p = Ast.Print.keyword p in
  match n with
  | None -> (p, a)
  | Some Ast.T.Input -> ("i_" ^ p, a)
  | Some Output -> ("o_" ^ p, a)
  | Some Reduction -> ("r_" ^ p, a)

let mn_atom (p, args) = (merge_namespace p, args)
let mn_lit = function
  | Ast.T.Lit (Okay a) -> Lit (Okay (mn_atom a))
  | Ast.T.Lit (Negation a) -> Lit (Negation (mn_atom a))
  | Ast.T.Lit (Predefined a) -> Lit (Predefined a)
let mn_rule (head, body) = (mn_atom head, List.map mn_lit body)

let x = "X"

let arg v = Ast.T.[Expr (Variable v)]
let input = Some Ast.T.Input
and ouput = Some Ast.T.Output

module GuidedVerify = struct
let input_base = merge_namespace Ast.T.(input, Base)
let direct            = mn_atom ((None,  Ast.T.Direct), [])
let input_certificate = mn_atom ((input, Ast.T.Certificate), [])
and ouput_certificate = mn_atom ((ouput, Ast.T.Certificate), [])
and input_basis       = mn_atom ((input, Ast.T.Basis),     arg x)
and input_domain      = mn_atom ((input, Ast.T.Domain),    arg x)
and input_candidate   = mn_atom ((input, Ast.T.Candidate), arg x)
and ouput_candidate   = mn_atom ((ouput, Ast.T.Candidate), arg x)
and input_instance    = mn_atom ((input, Ast.T.Instance),  arg x)
and ouput_instance    = mn_atom ((ouput, Ast.T.Instance),  arg x)
and input_solution    = mn_atom ((input, Ast.T.Solution),  arg x)
and ouput_solution    = mn_atom ((ouput, Ast.T.Solution),  arg x)

let direct_lit = T.Lit (T.Okay     direct)
and revers     = T.Lit (T.Negation direct)
let add_direct (head, body) = (head, direct_lit :: body)
let add_revers (head, body) = (head, revers :: body)

let program spec =
  let p1 = spec.Ast.T.input
  and r = spec.reduction
  and p2 = spec.output in
  let voc1 = Ast.add_namespace Ast.T.Input  (p1.Ast.T.vocabulary @ p1.solutioncheck)
  and voc2 = Ast.add_namespace Ast.T.Output (p2.Ast.T.vocabulary @ p2.solutioncheck) in
  let always_c = List.map mn_rule (voc1 @ voc2 @ r.Ast.T.mapping) in
  let direct_c = List.map add_direct (List.map mn_rule r.Ast.T.implies)
  and revers_c = List.map add_revers (List.map mn_rule r.Ast.T.implied) in
  always_c @ direct_c @ revers_c
end

module Solve = struct
let candidate = mn_atom Ast.T.((None, Candidate), arg x)
and solution = mn_atom Ast.T.((None, Solution), arg x)
and certificate = mn_atom Ast.T.((None, Certificate), [])

let program p i =
  List.map mn_rule (p.Ast.T.vocabulary @ p.Ast.T.solutioncheck @ i)
end

module Check = struct
let candidate = mn_atom Ast.T.((None, Candidate), arg x)
and solution = mn_atom Ast.T.((None, Solution), arg x)
and certificate = mn_atom Ast.T.((None, Certificate), [])

let program p i s =
  List.map mn_rule (p.Ast.T.vocabulary @ p.Ast.T.solutioncheck @ i @ s)
end

module Run = struct
let solution = mn_atom Ast.T.((None, Solution), arg x)
let program a i =
  List.map mn_rule (a @ i)
end
