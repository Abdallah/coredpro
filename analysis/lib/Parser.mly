%token <string> CONSTANT
%token <string> VARIABLE
%token <int> INT
%token BASE BASIS INSTANCE CANDIDATE CERTIFICATE SOLUTION
%token INPUT OUTPUT
%token FIRST // unipred
%token DISTINCT EQUAL LESSEQUAL LESSTHAN MOREEQUAL MORETHAN // binpred
%token TRIPLEEQ SOLUTIONCHECK DIRECT REVERS DOMAIN MAPPING VOCABULARY
%token RULE LPAREN RPAREN LBRACKET RBRACKET DOT COMMA AMPERSAND NAMESPACE
%token NOT
%token DIV PLUS MULT LOG MOD POW MINUS (*eop*)
%token EOF

%left MOD
%left PLUS MINUS
%left MULT DIV
%right POW LOG

%{
%}
%type <Ast.T.algorithm> algorithm
%type <Ast.T.instance> instance
%type <Ast.T.problem> problem
%type <Ast.T.reduction> reduction
%type <Ast.T.solution> solution
%start algorithm instance problem solution reduction
%%

%inline eoperator: | DIV { Ast.T.Div } | LOG { Ast.T.Log } | MOD { Ast.T.Mod } | MULT { Ast.T.Mult } | POW { Ast.T.Pow } | PLUS { Ast.T.Add } | MINUS { Ast.T.Sub }

expr:
| e1 = expr bo = eoperator e2 = expr { Ast.T.BinOp (bo, e1, e2) }
| LPAREN e = expr RPAREN { e }
| n = VARIABLE { Ast.T.Variable n }
| i = INT { Ast.T.Int i }
| MINUS e = expr { Ast.T.BinOp (Ast.T.Sub, Ast.T.Int 0, e) }

terms:
|  { [] }
|  LPAREN ts = separated_list(COMMA,term) RPAREN { ts }
term:
| symbol = CONSTANT ts = terms { Ast.T.Fact ((symbol, List.length ts), ts) }
| e = expr { Ast.T.Expr e }

%inline nonamespace:
| { None }
%inline namespace:
| { None }
| INPUT NAMESPACE { Some Ast.T.Input }
| OUTPUT NAMESPACE { Some Ast.T.Output }
noinputnamespace:
| { None }
| OUTPUT NAMESPACE { Some Ast.T.Output }
nooutputnamespace:
| { None }
| INPUT NAMESPACE { Some Ast.T.Input }
outputnamespace:
| OUTPUT NAMESPACE { Some Ast.T.Output }
inputnamespace:
| INPUT NAMESPACE { Some Ast.T.Input }
hasnamespace:
| INPUT NAMESPACE { Some Ast.T.Input }
| OUTPUT NAMESPACE { Some Ast.T.Output }


basis_atom(Namespace):
| n = Namespace BASIS LPAREN c = CONSTANT COMMA t = term RPAREN { ((n, Ast.T.Basis), [Ast.T.Fact ((c, 0), []); t]) }
certificate_atom(Namespace):
| n = Namespace CERTIFICATE { ((n, Ast.T.Certificate), []) }

any_atom(Namespace):
| n = Namespace predicate = CONSTANT ts = terms { ((n, Ast.T.Constant (predicate, List.length ts)), ts) }
make_rule(Head,Atom):
| head = Head DOT { (head, []) }
| head = Head RULE body = separated_list(AMPERSAND,topliteral(Atom)) DOT { (head, body) }

vocabulary_head:
| BASIS     LPAREN t = term RPAREN { ((None, Ast.T.Basis), [t]) }
| CANDIDATE LPAREN t = term RPAREN { ((None, Ast.T.Candidate), [t]) }
| DOMAIN    LPAREN t = term RPAREN { ((None, Ast.T.Domain), [t]) }
| a = any_atom(nonamespace) { a }
vocabulary_atom:
| BASE LPAREN t1 = term COMMA t2 = term RPAREN { ((None, Ast.T.Base), [t1; t2]) }
| predicate = CONSTANT ts = terms { ((None, Ast.T.Constant (predicate, List.length ts)), ts) }
solutioncheck_head:
| CERTIFICATE { ((None, Ast.T.Certificate), []) }
| a = any_atom(nonamespace) { a }
solutioncheck_atom:
| BASIS LPAREN t = term RPAREN { ((None, Ast.T.Basis), [t]) }
| BASE  LPAREN t1 = term COMMA t2 = term RPAREN { ((None, Ast.T.Base), [t1; t2]) }
| INSTANCE LPAREN t = term RPAREN { ((None, Ast.T.Instance), [t]) }
| SOLUTION LPAREN t = term RPAREN { ((None, Ast.T.Solution), [t]) }
| a = any_atom(nonamespace) { a }
problem:
| TRIPLEEQ VOCABULARY TRIPLEEQ
  vocabulary = list(make_rule(vocabulary_head,vocabulary_atom))
  TRIPLEEQ SOLUTIONCHECK TRIPLEEQ
  solutioncheck = list(make_rule(solutioncheck_head,solutioncheck_atom)) EOF { { Ast.T.vocabulary; solutioncheck } }

instance_atom:
| BASE     LPAREN t1 = term COMMA t2 = term RPAREN { ((None, Ast.T.Base), [t1; t2]) }
| INSTANCE LPAREN t  = term RPAREN { ((None, Ast.T.Instance), [t]) }
| a = any_atom(nonamespace) { a }
instance:
| l = list(make_rule(instance_atom,instance_atom)) EOF { l }

solution_head:
| SOLUTION LPAREN t = term RPAREN { ((None, Ast.T.Solution), [t]) }
| a = any_atom(nonamespace) { a }
solution_atom:
| BASE     LPAREN t1 = term COMMA t2 = term RPAREN { ((None, Ast.T.Base), [t1; t2]) }
| INSTANCE LPAREN t  = term RPAREN { ((None, Ast.T.Instance), [t]) }
| SOLUTION LPAREN t  = term RPAREN { ((None, Ast.T.Solution), [t]) }
| a = any_atom(nonamespace) { a }
solution:
| l = list(make_rule(solution_head,solution_atom)) EOF { l }

algorithm:
| s = solution { s }

mapping_head:
| n = outputnamespace BASE     LPAREN t1 = term COMMA t2 = term RPAREN { ((n, Ast.T.Base), [t1; t2]) }
| n = outputnamespace INSTANCE LPAREN t = term RPAREN { ((n, Ast.T.Instance), [t]) }
| n = outputnamespace CERTIFICATE { ((n, Ast.T.Certificate), []) }
| a = any_atom(noinputnamespace) { a }
mapping_atom:
| n = hasnamespace BASIS    LPAREN t = term RPAREN { ((n, Ast.T.Basis), [t]) }
| n = hasnamespace BASE     LPAREN t1 = term COMMA t2 = term RPAREN { ((n, Ast.T.Base), [t1; t2]) }
| n = hasnamespace INSTANCE LPAREN t = term RPAREN { ((n, Ast.T.Instance), [t]) }
| a = any_atom(namespace) { a }
implies_head:
| n = outputnamespace SOLUTION LPAREN t = term RPAREN { ((n, Ast.T.Solution), [t]) }
| a = any_atom(noinputnamespace) { a }
implies_atom:
| n = hasnamespace SOLUTION LPAREN t = term RPAREN { ((n, Ast.T.Solution), [t]) }
| a = mapping_atom { a }
implied_head:
| n = inputnamespace SOLUTION LPAREN t = term RPAREN { ((n, Ast.T.Solution), [t]) }
| a = any_atom(nooutputnamespace) { a }
implied_atom:
| n = hasnamespace SOLUTION LPAREN t = term RPAREN { ((n, Ast.T.Solution), [t]) }
| a = mapping_atom { a }
reduction:
| TRIPLEEQ MAPPING TRIPLEEQ
  mapping = list(make_rule(mapping_head,mapping_atom))
  TRIPLEEQ DIRECT TRIPLEEQ
  implies = list(make_rule(implies_head,implies_atom))
  TRIPLEEQ REVERS TRIPLEEQ
  implied = list(make_rule(implied_head,implied_atom)) EOF { { Ast.T.mapping; implies; implied } }

unipred:
| FIRST { Ast.T.First }
comparator:
| DISTINCT { Ast.T.Neq }
| EQUAL { Ast.T.Eq }
| LESSEQUAL { Ast.T.LessEq }
| LESSTHAN { Ast.T.LessThan }
| MOREEQUAL { Ast.T.MoreEq }
| MORETHAN { Ast.T.MoreThan }
predefined:
| t1 = term b = comparator t2 = term { Ast.T.Relation (b, t1, t2) }
| a = unipred LPAREN t = term RPAREN { Ast.T.Unary (a, t) }
literal(Atom):
| t = Atom { Ast.T.Okay t }
| NOT t = Atom { Ast.T.Negation t }
| p = predefined { Ast.T.Predefined p }
topliteral(Atom):
| l = literal(Atom) { Ast.T.Lit l }
