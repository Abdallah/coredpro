open Printf
let _ = ignore (printf)

module Opt = Optimization

module CLI = CommandLine
module CO = Completion.Ordered
module CC = Completion.Clark
module CU = Completion.Unidirectional
module C = CC
module FOL = FirstOrder
(*
(** [total_order namespace] returns a list of axiom corresponding to assuming a total ordering on the ground terms in [namespace]. Typically, [namespace = Ast.Input] or [namespace Ast.Output]. *)
let total_order n =
  let xt, yt, zt = ("x", FOL.Individual), ("y", FOL.Individual), ("z", FOL.Individual) in
  let x, y, z = Ast.T.Variable "x", Ast.T.Variable "y", Ast.T.Variable "z"
  and input = Ast.T.(n, Instance)
  and succ = assert false (*Ast.T.(Abstract Succ)*) in
  let input_x, input_y, input_z = FOL.Atom (input, [x]), FOL.Atom (input, [y]), FOL.Atom (input, [z])
  and succ_xy, succ_yx, succ_xz, succ_yz = FOL.Atom (succ, [x; y]), FOL.Atom (succ, [y; x]), FOL.Atom (succ, [x; z]), FOL.Atom (succ, [y; z])
  and equa_xy = FOL.Equa (x, y) in
  let transiti = FOL.{ name = sprintf "ord_%s_transiti" (Ast.Print.namespace n);
                       role = Axiom;
                       body = FLogic (Fora ([xt; yt; zt], Impl (Conj [input_x; input_y; input_z; succ_xy; succ_yz], succ_xz))) } in
  let antisymm = FOL.{ name = sprintf "ord_%s_antisymm" (Ast.Print.namespace n);
                       role = Axiom;
                       body = FLogic (Fora ([xt; yt], Impl (Conj [input_x; input_y; succ_xy; succ_yx], equa_xy))) } in
  let totality = FOL.{ name = sprintf "ord_%s_totality" (Ast.Print.namespace n);
                       role = Axiom;
                       body = FLogic (Fora ([xt; yt], Impl (Conj [input_x; input_y], Disj [succ_xy; succ_yx]))) } in
  [transiti; antisymm; totality]

let var_args_of_symbol prefix s =
  let gen = Generate.variable prefix in
  let names = List.map (fun _ -> gen (), FOL.Individual) (Utils.PList.range 0 (Ast.arity s)) in
  let vars = List.map (fun (v, _) -> Ast.T.Variable v) names in
  (names, vars)

let injective_symbol s =
  let (names1, vars1) = var_args_of_symbol "v" s
  and (names2, vars2) = var_args_of_symbol "w" s in
  let typed_names = names1 @ names2 in
  let vars = List.combine vars1 vars2 in
  let eq_term = FOL.Equa (Ast.T.Fact (s, vars1), Ast.T.Fact (s, vars2))
  and equas = FOL.Conj (List.map (fun vw -> FOL.Equa vw) vars) in
  if List.length vars > 0 then
    Some (FOL.{ name = "inj_" ^ Print.symbol s;
                role = Axiom;
                body = FLogic (Fora (typed_names, Impl (eq_term, equas))) })
  else None

let unique_names_pair (s1, s2) =
  let (names1, vars1) = var_args_of_symbol "v" s1
  and (names2, vars2) = var_args_of_symbol "w" s2 in
  let typed_names = names1 @ names2 in
  let uneq = FOL.Dist (Ast.T.Fact (s1, vars1), Ast.T.Fact (s2, vars2)) in
  FOL.{ name = sprintf "una_%s_%s" (Print.symbol s1) (Print.symbol s2);
        role = Axiom;
        body = FLogic (Fora (typed_names, uneq)) }

let unique_names symbols =
  let symbols = Symbol.Set.elements symbols in
  let pairs = Utils.PList.cross_product symbols symbols in
  let unique_pairs = List.filter (fun (s1, s2) -> compare s1 s2 < 0) pairs in
  List.map unique_names_pair unique_pairs

let implies_fact = FOL.Direct
and implied_fact = FOL.Nega FOL.Direct
let out_cert = FOL.(Atom (Ast.T.(Output, Certificate), []))
and in_cert  = FOL.(Atom (Ast.T.(Input, Certificate), []))
let presol_pred = Ast.T.Constant ("presol", 1)

let both_conjecture = FOL.Conj [FOL.(Impl (Conj [implies_fact; in_cert], out_cert)); FOL.(Impl (Conj [implied_fact; out_cert], in_cert))]

let presol namespace =
  let var = Ast.T.Variable "x" in
  let head = ((namespace, Ast.T.Solution), [var]) in
  let body = [Ast.T.Predefined (Ast.T.Direct (namespace = Ast.T.Input)); Ast.T.Okay ((namespace, presol_pred), [var])] in
  (head, body)

let clark combined =
  let input_preds = Ast.T.[(Input, Instance); (Input, Basis); (Input, presol_pred); (Output, presol_pred)] in
  let combined = presol Ast.T.Input :: presol Ast.T.Output :: combined in
  let base    = C.program input_preds combined in
  base

let objective triple o =
  let introduce (name, body) = { FOL.name; role = FOL.Axiom; body = FOL.FLogic body } in
  let conj = FOL.{ name = "target";
                   role = Conjecture;
                   body = FLogic both_conjecture } in
  let axiom = match o with
    | Opt.Both -> []
    | Opt.Implies -> [{ FOL.name = "direct"; role = FOL.Axiom; body = FOL.FLogic implies_fact }]
    | Opt.Implied -> [{ FOL.name = "revers"; role = FOL.Axiom; body = FOL.FLogic implied_fact }] in
  let prog = Utils.PList.map introduce triple in
  conj :: axiom @ prog

let declare_measure_types combined =
  let predicates = Desugared.predicates combined in
  let declare_one pred =
    let args = List.map (fun _ -> FOL.Individual) (Utils.PList.range 0 (Ast.arity_pred pred)) in
    FOL.{ name = Print.predicate pred ^ "t";
          role = Type;
          body = FType (pred, args, Int) } in
  List.map declare_one (Pred.Set.filter_mapl C.measure_pred predicates)

let make_axioms direction spec combined =
  if false then eprintf "%s\n" (Ast.Print.program combined);
  let symbols = Desugared.symbols combined in
  let types = declare_measure_types combined in
  let injectives = Symbol.Set.filter_mapl injective_symbol symbols in
  let uoas = unique_names symbols in
  let program = clark combined in
  let prog = objective program direction in
  let order = total_order Ast.T.Input @ total_order Ast.T.Output in
  let order = [] in
  types @ prog @ order @ injectives @ uoas

(*let test_wellformedness spec =
  let input_problem, _, output_problem = Combine.do_types2 spec in
  let input_preds1 = Ast.T.[Qualified (Input, Base Int); Qualified (Input, Base Tbd)]
  and input_preds2 = Ast.T.[Qualified (Output, Base Int); Qualified (Output, Base Tbd)] in
  let i = C.program input_preds1 input_problem
  and o = C.program input_preds2 output_problem in
  i @ o*)

let type_axioms spec types =
  let input_problem, _, output_problem = types in
  let input_preds1 = Ast.T.[Input, Basis]
  and input_preds2 = Ast.T.[Output, Basis] in
  let i = CC.equivalences input_preds1 input_problem
  and o = CC.equivalences input_preds2 output_problem in
  let turn_to_impl (pred, vars, left, right) =
    let name = sprintf "%st" (FOL.Print.predicate pred)
    and body = FOL.(FLogic (Fora (vars, Impl (left, right)))) in
    FOL.{ name; role = Axiom; body } in
  List.map turn_to_impl (i @ o)

let transform_to_asp spec combined types =
  let (t_inp, tr, t_out) = types in
  AnswerSetProgramming.prog ((t_inp, tr, t_out), combined)

let transform_to_fol spec direc combined types =
  let red_axioms = make_axioms direc spec combined in
  if false then Combine.do_types spec;
  let typ_axioms = type_axioms spec types in
  FOL.Print.program (typ_axioms @ red_axioms)

let make_specification options =
  let spec = match options.CLI.input_names with
    | name1 :: name2 :: name3 :: [] -> Parse.from_files () name1 name2 name3
    | _ -> assert false in
  let combined = Combine.make spec
  and types = Combine.do_types2 spec in
  if not options.CLI.asp then transform_to_fol spec options.CLI.direction combined types
  else transform_to_asp spec combined types
*)
(*let make_specification options =
  let spec = match options.CLI.input_names with
    | name1 :: name2 :: name3 :: [] -> Parse.from_files () name1 name2 name3
    | _ -> assert false in
  assert options.CLI.asp;
  AnswerSetProgramming.verify spec.Ast.T.input spec.reduction spec.output*)
(*  let combined = Combine.make spec
  and types = Combine.do_types2 spec in
  if not options.CLI.asp then transform_to_fol spec options.CLI.direction combined types
  else transform_to_asp spec combined types
*)
