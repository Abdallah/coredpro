type program = (string * FirstOrder.t) list

module type S = sig
  val measure_pred : Pred.t -> Pred.t option

  (** [program prefix inputs p] returns the completion of the input Datalog program, where axiom names are prefixed with [prefix] and predicates in [inputs] are considered input predicates. *)
  val program : Pred.t list -> Ast.T.program -> program
end

module Clark : sig
include S
val equivalences : Pred.t list -> Ast.T.program -> (Ast.T.predicate * (string * FirstOrder.tfftype) list * FirstOrder.t * FirstOrder.t) list
end

module Ordered : S

module Unidirectional : S
