(** This module allows creating new predicates and rules to generate them and avoid duplications. *)

type 'a t

val variable : string -> (unit -> string)
val predicate : Ast.T.namespace option -> string -> (int -> Ast.T.predicate)
val empty : (int -> Ast.T.predicate) -> Ast.T.predicate t
(*val add_rule : Ast.T.predicate t -> string list -> Ast.T.literal list -> Ast.T.predicate t * Ast.T.atomic*)
val get : Ast.T.predicate t -> Ast.T.rule list
