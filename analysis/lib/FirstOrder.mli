type role = Axiom | Conjecture | Type

type tfftype = Bool | Individual | Int
type tvariable = Variable.t * tfftype
type maybeint = (Variable.t, int) Either.t

type t =
  | Direct
  | Conj of t list
  | Disj of t list
  | Atom of Ast.T.atomic
  | Exis of (tvariable list * t)
  | Fora of (tvariable list * t)
  | Nega of t
  | Predef of Ast.T.predefined
  | Dist of (Ast.T.term * Ast.T.term)
  | Equa of (Ast.T.term * Ast.T.term)
  | Less of maybeint * maybeint
  | Lessp of (Ast.T.atomic * Ast.T.atomic)
  | Impl of (t * t)
  | Equi of (t * t)

type atom_typing = Ast.T.predicate * tfftype list * tfftype

type formula = FLogic of t | FType of atom_typing

type axiom =
  { name : string;
    role : role;
    body : formula; }

type program = axiom list

module Print :
sig
  val t : t  -> string
  val symbol : Ast.T.symbol -> string
  val predicate : Ast.T.predicate -> string
  val axiom : axiom  -> string
  val formula : formula -> string
  val program : program -> string
end

val map_predicate : (Pred.t -> Pred.t) -> t -> t
