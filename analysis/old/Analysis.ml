open Printf

module C = Utils.Choice

let rec symbols_term : type a. a Ast.term -> Symbol.Set.t = function
  | Ast.Variable _ -> Symbol.Set.empty
  | Ast.Fact (cst, args) -> Symbol.Set.add cst (Symbol.Set.maps_union symbols_term args)
let symbols_atomic (_, args) = Symbol.Set.maps_union symbols_term args
let symbols_literal : type a b. (a, b) Desugared.literal -> Symbol.Set.t = function
  | Desugared.Okay at | Desugared.Negation at -> symbols_atomic at
  | Desugared.Abstract (_, ts) -> Symbol.Set.maps_union symbols_term ts
let symbols_rule (head, body) = Symbol.Set.union (symbols_atomic head) (Symbol.Set.maps_union symbols_literal body)
let symbols prog = Symbol.Set.maps_union symbols_rule prog

let predicates_literal : type a. (Ast.predicate, a) Desugared.literal -> Pred.Set.t = function
  | Desugared.Okay at | Desugared.Negation at -> Pred.Set.singleton (fst at)
  | Desugared.Abstract _ -> Pred.Set.empty
let predicates_rule (head, body) = Pred.Set.add (fst head) (Pred.Set.maps_union predicates_literal body)
let predicates prog = Pred.Set.maps_union predicates_rule prog

let dep_graph_lit : type a. Ast.predicate -> Pred.Set.t Pred.Map.t -> (Ast.predicate, a) Desugared.literal -> Pred.Set.t Pred.Map.t = fun predicate map -> function
  | Desugared.Okay (cst, _) | Desugared.Negation (cst, _) -> Pred.Map.update predicate (Pred.Set.add cst) Pred.Set.empty map
  | Desugared.Abstract _ -> map
let dep_graph_rule map (head, body) = List.fold_left (dep_graph_lit (fst head)) map body
let dep_graph rules =
  let map = Pred.Map.init Pred.Set.empty (Pred.Set.elements (predicates rules)) in
  List.fold_left dep_graph_rule map rules

let transitive_dep_graph rules =
  let graph = dep_graph rules in
  let deps set = Pred.Set.union_maps (fun predicate -> Pred.Map.find_default predicate Pred.Set.empty graph) set in
  let deps' set = Pred.Set.union set (deps set) in
  let rec fixpoint acc_g =
    let new_g = Pred.Map.map deps' acc_g in
    if Pred.Map.compare Pred.Set.compare acc_g new_g = 0 then acc_g else fixpoint new_g in
  fixpoint graph

let cst_from_literal : type a. (Ast.predicate, a) Desugared.literal -> Pred.Set.t = function
  | Desugared.Okay (cst, _)
  | Desugared.Negation (cst, _) -> Pred.Set.singleton cst
  | Desugared.Abstract _ -> Pred.Set.empty

let cst_from_rule ((cst, _), lits) = Pred.Set.add cst (Pred.Set.maps_union cst_from_literal lits)

let start_map rules (f : Ast.predicate -> 'a) : 'a Pred.Map.t =
  let add t = Pred.Map.add t (f t) in
  let set = Pred.Set.maps_union cst_from_rule rules in
  Pred.Set.fold add set Pred.Map.empty

let from_lit : type b. ('a, b) Desugared.literal -> ('a, 'a) C.t option = function
  | Desugared.Okay (cst, _) -> Some (C.Right cst)
  | Desugared.Negation (cst, _) -> Some (C.Left cst)
  | Desugared.Abstract _ -> None

let get_cst_literals ((cst, _), literals) =
  let negs, poss = C.partition_id (Utils.PList.filter_map from_lit literals) in
  (cst, poss, negs)

let new_value f old_map (cst, poss, negs) =
  let old = Pred.Map.find cst old_map in
  let get_val c = Pred.Map.find c old_map in
  let poss = Utils.PList.map get_val poss
  and negs = Utils.PList.map get_val negs in
  Pred.Map.add cst (f old poss negs) old_map

let rec fixpoint_aux rules start f =
  let result = List.fold_left (new_value f) start rules in
  if Pred.Map.equal (=) start result then result
  else fixpoint_aux rules result f

let fixpoint prog strat f =
  let rules = Utils.PList.map get_cst_literals prog in
  try fixpoint_aux rules strat f with exn -> eprintf "%s\n%!" (Printexc.to_string exn); assert false
