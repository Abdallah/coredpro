val symbols : ('a, 'b) Desugared.program -> Symbol.Set.t
val predicates : (Ast.predicate, 'a) Desugared.program -> Pred.Set.t
val dep_graph : (Ast.predicate, 'a) Desugared.program -> Pred.Set.t Pred.Map.t
val transitive_dep_graph : (Ast.predicate, 'a) Desugared.program -> Pred.Set.t Pred.Map.t
val start_map : (Ast.predicate, 'a) Desugared.program -> (Ast.predicate -> 'b) -> 'b Pred.Map.t

(* old positive_lits negative_lits *)
val fixpoint : (Ast.predicate, 'a) Desugared.program -> 'b Pred.Map.t -> ('b -> 'b list-> 'b list -> 'b) -> 'b Pred.Map.t
