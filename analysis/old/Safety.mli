(*
(** A clause is safe if all variables occur in at least one positive literal. *)

val program : 'a Ast.T.program -> (string * 'a Ast.T.rule) option

(** A clause is sorted safe if all variables occur in at least one positive literal before occuring in any non positive literal. *)
val sorted_clause : 'a Ast.T.rule -> bool
val sorted : 'a Ast.T.program -> bool

val sort_literals : 'a Ast.T.literal list -> 'a Ast.T.literal list
val sort_clause : 'a Ast.T.rule -> 'a Ast.T.rule
val sort : 'a Ast.T.program -> 'a Ast.T.program
*)
