echo "      3sat 3col"
coredpro --direction implies ../reductions/3sat_3coloring ../problems/3sat.problem ../problems/3coloring.problem > test-output/3sat-3coloring.tptp
echo "      vampire:"
../extern/vampire --statistics none --time_statistics on --forced_options proof=off --time_limit 300s --mode casc test-output/3sat-3coloring.tptp
