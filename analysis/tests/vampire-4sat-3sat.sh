echo "      4sat 3sat"
coredpro --direction implies ../reductions/4sat_3sat ../problems/4sat.problem ../problems/3sat.problem > test-output/4sat-3sat.tptp
echo "      vampire:"
../extern/vampire --statistics none --time_statistics on --forced_options proof=off --time_limit 900s --mode casc test-output/4sat-3sat.tptp # --statistics brief
