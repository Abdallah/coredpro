echo "      3sat 4sat"
coredpro --direction both ../reductions/3sat_4sat ../problems/3sat.problem ../problems/4sat.problem > test-output/3sat-4sat.tptp
echo "      vampire:"
../extern/vampire --statistics none --time_statistics on --forced_options proof=off --time_limit 900s --mode casc test-output/3sat-4sat.tptp
