open Printf
open Coredpro

module ASP = AnswerSetProgramming
module CLI = CommandLine
module T = Transformations

(*let preprocess options = T.to_optimized options !(options.CLI.input_name)*)

(*let main options =
  let { CLI.verbose; output_name; _ } = options in
  if !verbose >= 2 then eprintf "%s\n" (CLI.print options);
  let process () = T.make_specification options in
  let (result, time) = Utils.Time.measure process () in
  if output_name = "" then printf "%s\n" result
  else Utils.Print.to_file output_name result;
  if !verbose >= 1 then eprintf "time to compile: %.3f sec\n%!" time*)

let _ =
  let options = CLI.get () in
  match options with
  | CLI.Verify (true, help, i, r, o) ->
     let i = Parse.problem i
     and r = Parse.reduction r
     and o = Parse.problem o in
     let verif = if help then ASP.guided_verify else ASP.verify in
     printf "%s" (verif i r o)
  | CLI.Solve (p, i) ->
     let p = Parse.problem p
     and i = Parse.instance i in
     printf "%s" (ASP.solve p i)
  | CLI.Check (p, i, s) ->
     let p = Parse.problem p
     and i = Parse.instance i
     and s = Parse.solution s in
     printf "%s" (ASP.check p i s)
  | CLI.Run (a, i) ->
     let a = Parse.algorithm a
     and i = Parse.instance i in
     printf "%s" (ASP.run a i)
  | _ -> assert false
(*  try main options
  with | Invalid_argument str -> eprintf "Bad input: %s\n%!" str; exit 1
       | exn -> eprintf "%s\n%!" (Printexc.to_string exn); exit 2*)
