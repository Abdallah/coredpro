set -v
coredpro --check --problem=../pv2/3sat.problem      --instance=../iv2/3sat.0.instance      --solution=../sv2/3sat.0.correct.solution > mytest.lp && clingo -V0 mytest.lp
coredpro --check --problem=../pv2/3sat.problem      --instance=../iv2/3sat.1.instance      --solution=../sv2/3sat.1.correct.solution > mytest.lp && clingo -V0 mytest.lp
coredpro --check --problem=../pv2/3coloring.problem --instance=../iv2/3coloring.0.instance --solution=../sv2/3coloring.0.correct.solution > mytest.lp && clingo -V0 mytest.lp

coredpro --check --problem=../pv2/3sat.problem      --instance=../iv2/3sat.0.instance      --solution=../sv2/3sat.0.incorrect.solution > mytest.lp && clingo -V0 mytest.lp
coredpro --check --problem=../pv2/3sat.problem      --instance=../iv2/3sat.1.instance      --solution=../sv2/3sat.1.incorrect.solution > mytest.lp && clingo -V0 mytest.lp
coredpro --check --problem=../pv2/3coloring.problem --instance=../iv2/3coloring.0.instance --solution=../sv2/3coloring.0.incorrect.solution > mytest.lp && clingo -V0 mytest.lp
coredpro --check --problem=../pv2/3coloring.problem --instance=../iv2/3coloring.1.instance --solution=../sv2/3coloring.1.incorrect.solution > mytest.lp && clingo -V0 mytest.lp
