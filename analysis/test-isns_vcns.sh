mkdir -p test
echo "      isns vcns"
./coredpro --direction both ../reductions/independent-set-nosize_vertex-cover-nosize ../problems/independent-set-nosize.problem ../problems/vertex-cover-nosize.problem > test/independent-set-nosize_vertex-cover-nosize.tptp
echo "      vampire:"
../extern/vampire --statistics brief --time_statistics on --proof off --time_limit 300s --mode casc test/independent-set-nosize_vertex-cover-nosize.tptp

