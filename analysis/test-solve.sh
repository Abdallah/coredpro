set -v
coredpro --solve --problem=../pv2/3sat.problem      --instance=../iv2/3sat.0.instance      > mytest.lp && clingo -V0 mytest.lp
coredpro --solve --problem=../pv2/3sat.problem      --instance=../iv2/3sat.1.instance      > mytest.lp && clingo -V0 mytest.lp
coredpro --solve --problem=../pv2/3coloring.problem --instance=../iv2/3coloring.0.instance > mytest.lp && clingo -V0 mytest.lp
coredpro --solve --problem=../pv2/3coloring.problem --instance=../iv2/3coloring.1.instance > mytest.lp && clingo -V0 mytest.lp
