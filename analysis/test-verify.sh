set -v
coredpro --verify --input=../pv2/3sat.problem --reduction=../rv2/3sat_4sat.reduction         --output=../pv2/4sat.problem         > mytest.lp && clingo mytest.lp -V0 --const r_base=10
coredpro --verify --input=../pv2/3sat.problem --reduction=../rv2/3sat_4sat.1.reduction         --output=../pv2/4sat.problem         > mytest.lp && clingo mytest.lp -V0 --const r_base=11
#coredpro --verify --input=../pv2/4sat.problem --reduction=../rv2/4sat_3sat.reduction         --output=../pv2/3sat.problem         > mytest.lp && clingo mytest.lp -V0 --const r_base=2
coredpro --verify --input=../pv2/3sat.problem --reduction=../rv2/3sat_3coloring.reduction    --output=../pv2/3coloring.problem    > mytest.lp && clingo mytest.lp -V0 --const r_base=4
coredpro --verify --input=../pv2/3sat.problem --reduction=../rv2/3sat_3coloring.1.reduction  --output=../pv2/3coloring.problem    > mytest.lp && clingo mytest.lp -V0 --const r_base=4
coredpro --verify --input=../pv2/sat.problem  --reduction=../rv2/sat_3sat.reduction          --output=../pv2/3sat.problem         > mytest.lp && clingo mytest.lp -V0 --const r_base=6
coredpro --verify --input=../pv2/3sat.problem --reduction=../rv2/3sat_3sat-ordered.reduction --output=../pv2/3sat-ordered.problem > mytest.lp && clingo mytest.lp -V0 --const r_base=13
