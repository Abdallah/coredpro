(*
  Title: Equivalence of Reductions
  Author: Christine Rizkallah
*)
theory Reductions
imports
  Main

begin

lemma finite_reduction_equiv_forward:
  fixes SP :: "('a \<times> 'b) set" 
  fixes SP' ::  "('c \<times> 'd) set"
  fixes f :: "'a \<Rightarrow> 'c"
  assumes "finite SP"
  assumes "(\<forall>x. (\<exists>y. (x,y)\<in>SP) \<longrightarrow> (\<exists>y'. (f x,y')\<in>SP'))"
  shows "\<exists> (g:: 'a \<Rightarrow> 'b \<Rightarrow> 'd). 
            (\<forall>x y. (x,y)\<in> SP \<longrightarrow> (f x, g x y)\<in> SP')"
using assms
proof (induct SP rule:finite_induct) 
 case empty
  then show ?case by simp
next
  case (insert a A)
  then show ?case 
    apply simp
    apply (rule_tac x=
        "\<lambda>x y. if (x,y) = a then SOME y'. (f x, y') \<in> SP' else 
                  (SOME y'. \<exists>g. y' = g x y \<and> (f x, g x y) \<in> SP')" in exI)
    apply clarsimp
    apply (rule conjI) 
    apply (erule_tac x=x in allE)
     apply (rule impI)
     apply (subgoal_tac "(\<exists>y'. (f x, y') \<in> SP')")
      apply clarsimp
      apply (rule someI, blast+) 
    apply clarsimp 
    apply (subgoal_tac "\<forall>x. (\<exists>y. (x, y) \<in> A) \<longrightarrow> (\<exists>y'. (f x, y') \<in> SP')")
     apply simp
     apply (erule exE)
    by (rule someI2, blast+) 
  qed

lemma finite_reduction_equiv_backward:
  fixes SP :: "('a \<times> 'b) set" 
  fixes SP' ::  "('c \<times> 'd) set"
  fixes f :: "'a \<Rightarrow> 'c"
   assumes "finite SP'"
  assumes "(\<forall>x. (\<exists>y'. (f x,y')\<in>SP')\<longrightarrow> (\<exists>y. (x,y)\<in>SP))"
  shows "\<exists>(h:: 'a \<Rightarrow> 'd \<Rightarrow> 'b). 
            (\<forall>x y'. (f x, y')\<in> SP' \<longrightarrow> (x,h x y')\<in> SP)"
using assms
proof (induct SP' rule:finite_induct) 
 case empty
  then show ?case by simp
next
  case (insert a A)
  then show ?case 
    apply simp
    apply (rule_tac x=
        "\<lambda>x y. if (f x,y) = a then SOME y'. (x, y') \<in> SP else 
                  (SOME y'. \<exists>h. y' = h x y \<and> (x, h x y) \<in> SP)" in exI)
    apply clarsimp
    apply (rule conjI) 
    apply (erule_tac x=x in allE)
     apply (rule impI)
     apply (subgoal_tac "(\<exists>y'. (x, y') \<in> SP)")
      apply clarsimp
      apply (rule someI, blast+) 
    apply clarsimp 
    apply (subgoal_tac "\<forall>x. (\<exists>y'. (f x, y') \<in> A) \<longrightarrow> (\<exists>y. (x, y) \<in> SP)")
     apply simp
     apply (erule exE)
    by (rule someI2, blast+) 
qed

lemma finite_reduction_equiv':
  fixes SP :: "('a \<times> 'b) set" 
  fixes SP' ::  "('c \<times> 'd) set"
  fixes f :: "'a \<Rightarrow> 'c"
  assumes "finite SP"
  assumes "finite SP'"
  assumes "(\<forall>x. (\<exists>y. (x,y)\<in>SP) \<longrightarrow> (\<exists>y'. (f x,y')\<in>SP'))"
  assumes "(\<forall>x. (\<exists>y'. (f x,y')\<in>SP')\<longrightarrow> (\<exists>y. (x,y)\<in>SP))"
  shows "\<exists>(h:: 'a \<Rightarrow> 'd \<Rightarrow> 'b) (g:: 'a \<Rightarrow> 'b \<Rightarrow> 'd). 
            (\<forall>x y y'. (((x,y)\<in> SP \<longrightarrow> (f x, g x y)\<in> SP') \<and> 
                      ((f x, y')\<in> SP' \<longrightarrow> (x,h x y')\<in> SP)))"
  using  finite_reduction_equiv_forward[OF assms(1) assms (3)] 
         finite_reduction_equiv_backward[OF assms(2) assms(4)]
  by metis

lemma finite_reduction_equiv:
  fixes SP :: "('a \<times> 'b) set" 
  fixes SP' ::  "('c \<times> 'd) set"
  fixes f :: "'a \<Rightarrow> 'c"
  assumes "finite SP"
  assumes "finite SP'"
  assumes "(\<forall>x. (\<exists>y. (x,y)\<in>SP) \<longleftrightarrow> (\<exists>y'. (f x,y')\<in>SP'))"
  shows "\<exists>(h:: 'a \<Rightarrow> 'd \<Rightarrow> 'b) (g:: 'a \<Rightarrow> 'b \<Rightarrow> 'd). 
            (\<forall>x y y'. (((x,y)\<in> SP \<longrightarrow> (f x, g x y)\<in> SP') \<and> 
                      ((f x, y')\<in> SP' \<longrightarrow> (x,h x y')\<in> SP)))"
  using finite_reduction_equiv'[OF assms(1,2)] assms(3)
  by simp

lemma demorgans_exI: "\<not> (\<forall> x. \<not> P x) \<Longrightarrow> \<exists> x. P x" 
  by simp



lemma reduction_equiv_forward:
  fixes SP :: "('a \<times> 'b) set" 
  fixes SP' ::  "('c \<times> 'd) set"
  fixes f :: "'a \<Rightarrow> 'c"
  assumes "(\<forall>x. (\<exists>y. (x,y)\<in>SP) \<longrightarrow> (\<exists>y'. (f x,y')\<in>SP'))"
  shows "\<exists> (g:: 'a \<Rightarrow> 'b \<Rightarrow> 'd). 
            (\<forall>x y. (x,y)\<in> SP \<longrightarrow> (f x, g x y)\<in> SP')"
  apply (insert assms)
    apply (rule_tac x=
        "\<lambda>x y. SOME y'. (f x, y') \<in> SP'" in exI) 
  apply clarsimp
  apply (erule_tac x=x in allE)
  apply (erule impE) 
   apply blast
  apply clarsimp 
  apply (rule_tac x=y' in someI) 
  by simp

lemma reduction_equiv_backward:
  fixes SP :: "('a \<times> 'b) set" 
  fixes SP' ::  "('c \<times> 'd) set"
  fixes f :: "'a \<Rightarrow> 'c"
  assumes "(\<forall>x. (\<exists>y'. (f x,y')\<in>SP')\<longrightarrow> (\<exists>y. (x,y)\<in>SP))"
  shows "\<exists>(h:: 'a \<Rightarrow> 'd \<Rightarrow> 'b). 
            (\<forall>x y'. (f x, y')\<in> SP' \<longrightarrow> (x,h x y')\<in> SP)"
  apply (insert assms)
    apply (rule_tac x=
        "\<lambda>x y'. SOME y. (x, y) \<in> SP" in exI) 
  apply clarsimp
  apply (erule_tac x="x" in allE)
  apply (erule impE) 
   apply blast
  apply clarsimp 
  apply (rule_tac x=y in someI) 
  by simp

lemma reduction_equiv':
  fixes SP :: "('a \<times> 'b) set" 
  fixes SP' ::  "('c \<times> 'd) set"
  fixes f :: "'a \<Rightarrow> 'c"
  assumes "(\<forall>x. (\<exists>y. (x,y)\<in>SP) \<longrightarrow> (\<exists>y'. (f x,y')\<in>SP'))"
  assumes "(\<forall>x. (\<exists>y'. (f x,y')\<in>SP')\<longrightarrow> (\<exists>y. (x,y)\<in>SP))"
  shows "\<exists>(h:: 'a \<Rightarrow> 'd \<Rightarrow> 'b) (g:: 'a \<Rightarrow> 'b \<Rightarrow> 'd). 
            (\<forall>x y y'. (((x,y)\<in> SP \<longrightarrow> (f x, g x y)\<in> SP') \<and> 
                      ((f x, y')\<in> SP' \<longrightarrow> (x,h x y')\<in> SP)))"
  using  reduction_equiv_forward[OF assms (1)] 
         reduction_equiv_backward[OF assms(2)]
  by metis

lemma reduction_equiv:
  fixes SP :: "('a \<times> 'b) set" 
  fixes SP' ::  "('c \<times> 'd) set"
  fixes f :: "'a \<Rightarrow> 'c"
  assumes "(\<forall>x. (\<exists>y. (x,y)\<in>SP) \<longleftrightarrow> (\<exists>y'. (f x,y')\<in>SP'))"
  shows "\<exists>(h:: 'a \<Rightarrow> 'd \<Rightarrow> 'b) (g:: 'a \<Rightarrow> 'b \<Rightarrow> 'd). 
            (\<forall>x y y'. (((x,y)\<in> SP \<longrightarrow> (f x, g x y)\<in> SP') \<and> 
                      ((f x, y')\<in> SP' \<longrightarrow> (x,h x y')\<in> SP)))"
  by (rule reduction_equiv') (simp add: assms)+

end
